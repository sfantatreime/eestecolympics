var express = require('express');
var http = require('http');
var path = require('path');
var app = express();
var monk = require('monk');
var mongoUri = '192.168.1.12:27017/userdb';
var db = monk(mongoUri);
var expressSession = require('express-session');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var crypto = require('crypto');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var cors = require('cors');
var nodemailer = require('nodemailer');
var Q = require('q');
var transporter = nodemailer.createTransport({ //aici se initializeaza mailer-ul. Inlocuieste user cu o adresa de gmail si pass cu parola contului pt ca emailerul sa fie valid.
    //in caz de alta adresa, trebuie inlocuit si service. Vezi lista urmatoare pentru serviciile disponibile : https://github.com/andris9/nodemailer-wellknown#supported-services
    service: 'gmail',
    auth: {
        user: 'dummy@gmail.com',
        pass: '1234'
    }
});

app.use(cors());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(expressSession({ secret: process.env.SESSION_SECRET || 'secret',
                         resave: false,
                         saveUninitialized: false}));
app.use(passport.initialize());
app.use(passport.session());

passport.use(new LocalStrategy(function(username, password, done) {
    var collection = db.get("users");
    collection.findOne({ username: username }, function(err, user) {
        if (err) {
            return done(err);
        }
        if (!user) {
          return done(null, false, { message: 'Incorrect username.' });
        }
        else {
            crypto.pbkdf2(password, 'ala-bala-portocala', 10000, 20, function(err, derivedKey) {
                if ( err ) {
                    return done(err);
                }
                else {
                    if (derivedKey.toString() !== user.password.buffer.toString()) {
                        return done(null, false, { message: 'Incorrect password.' });
                    }
                    return done(null, user);
                }
            });
        }
    });
}));

passport.serializeUser(function(user, done) {
    done(null, user);
});
passport.deserializeUser(function(user, done) {
    done(null, user);
});

// all environments
app.set('port', 5555);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.static(path.join(__dirname, '/public')));
var log4js = require('log4js'); // include log4js
log4js.configure({ // configure to use all types in different files.
    appenders: [
        {   type: "file",
            filename: "./app/deploy/logs/error.log", // specify the path where u want logs folder info.log
            category: 'error',
        }
    ]
});

var errorLogger = log4js.getLogger('error'); // initialize the error logger

function refreshAuthDate(token) {
    var users = db.get('users');
    users.update({token : token}, {$set: {authDate : new Date()}});
} //face refresh la ora authentificarii de fiecare data cand userul face un request

//editeaza o anumita proprietate a unui user in baza de date
app.get('/edit', function(request, result) {
    var token = request.query.token;
    var key = request.query.key;
    var value = request.query.value;
    var users = db.get('users');
    refreshAuthDate(token);
    if (key === 'firstName') {
        users.update({token : token}, {$set: {firstName : value}},function(err) {
            errorLogger.error(err);
        });
        result.send("firstName_changed");
    }
    else if(key === 'lastName') {
        users.update({token : token}, {$set: {lastName : value}});
        result.send("lastName_changed");
    }
    else if(key === 'email') {
        users.update({token : token}, {$set: {email : value}});
        result.send("email_changed");
    }
    else if(key === 'phoneNumber') {
        users.update({token : token}, {$set: {phoneNumber : value}});
        result.send("phoneNumber_changed");
    }
    else if(key === 'address') {
        users.update({token : token}, {$set: {address : value}});
        result.send("address_changed");
    }
    else if(key === 'dateOfBirth') {
        var dateOfBirth = value;
        try {
            var array = dateOfBirth.split('-');
            var auxDate = new Date(parseInt(array[2]), parseInt(array[1]) - 1, parseInt(array[0]));
            if ( Object.prototype.toString.call(auxDate) === "[object Date]" || isNaN(auxDate.getTime) ) {
                result.send("invalid_dob");
                return;
            }
            if ( auxDate.getFullYear() >= 2007 || auxDate.getFullYear() <= 1890 || auxDate.getMonth() <= 0 || auxDate.getMonth() >= 13 || auxDate.getDate() <= 0 || auxDate.getDate() >= 32 )
            {
                result.send("invalid_dob");
                return;
            }
            users.update({token : token}, {$set: {dateOfBirth : value}});
            result.send("dateOfBirth_changed");
        } catch (e) {
            result.send("invalid_dob");
            return;
        }
    }
    else if(key === 'sex') {
        if ( value === 'M' || value === 'F' ) {
            users.update({token : token}, {$set: {sex : value}});
            result.send("sex_changed");
        }
        else {
            result.send("invalid_sex");
        }
    }
    else if(key === 'slots') {
        for ( var i = 0 ; i < 7 ; i++ ) {
            value[i] = JSON.parse(value[i]);
        }
        users.update({token : token}, {$set: {slots : value}});
        result.send("slots_changed");
    }
    else if(key === 'favoriteSports') {
        try {
            if ( value instanceof Array ) {
                for ( var j = 0 ; j < value.length ; j++ ) {
                    value[j] = JSON.parse(value[j]);
                }
            }
            else if ( value instanceof Object) {
                value = [value];
            }
        } catch(e) {
            errorLogger.error(e);
        }
        users.update({token : token}, {$set: {favoriteSports : value}});
        result.send("favoriteSports_changed");
    }
    else {
        result.send("invalid_key");
    }
});

//Easter Egg
app.get('/secret', function(request, result) {
    result.send("Gheorghe Alin-Gabriel si Cristache Gabriel © Copyright");
});


//schimba parola
app.post("/edit", function(request, result) {
    var oldPassword = request.body.oldPassword;
    var newPassword = request.body.newPassword;
    var token = request.body.token;
    refreshAuthDate(token);
    var users = db.get('users');
    if(oldPassword !== undefined && newPassword !== undefined)
    {
        users.findOne({token : token}, function(err, user) {
            if ( err ) { errorLogger.error(err); }
            else {
                if (user === null) { 
                    result.send("token_error or user_logged_out");
                    return;
                }
                crypto.pbkdf2(oldPassword, 'ala-bala-portocala', 10000, 20, function(err, derivedKey) { //face hash parolei (nu e tinuta in clar in baza de date)
                    if ( err ) { errorLogger.error(err); }
                    else {
                        if ( derivedKey.toString() === user.password.buffer.toString() ) {
                            crypto.pbkdf2(newPassword, 'ala-bala-portocala', 10000, 20, function(err, hashedPassword) {
                                if(err) { errorLogger.error(err); }
                                else {
                                    users.update({token:token}, {$set: {password: hashedPassword}}, function(err) {
                                        if (err) { errorLogger.error(err); }
                                    });
                                    result.send("password_changed");
                                }
                            });
                        }
                        else {
                            result.send("old_password_doesn't_match");
                        }
                    }
                });
            }
        });
    }
    else {
        result.send("invalid_password");
    }
});

//la fiecare 60 secunde, delogheaza toti userii care sunt inactivi de mai mult de 6 ore
/*setInterval(function() {
    var users = db.get('users');
    users.find({}, function(err, allUsers) {
        if ( err ) { errorLogger.error(err); }
        else {
            if ( allUsers === undefined || allUsers === null || allUsers.length === 0 ) { return; }
            for ( var i = 0 ; i < allUsers.length ; i++ ) {
                var diff = (new Date() - allUsers[i].authDate)/1000/60/60;
                if ( diff >= 6 ) {
                    var id = allUsers[i]._id;
                    users.update({_id : id}, {$set: {token : "-1"}});
                }
            }
        }
    });
}, 1000*1);*/

//in functie de token, spune ce tip de utilizator este : admin, user sau unauthorized
app.get('/permission', function(request, result) {
    var token = request.query.token;
    var foundInUsers = false;
    var Q = require('q');
    var deferred = Q.defer();
    if ( token === '-1' ) {
        result.send({type:'unauthorized'});
        return;
    }
    (function() {
        db.get('users').findOne({token : token}, function(err, user) {
            if ( err ) { errorLogger.error(err); }
            else {
                if ( user !== undefined && user !== null ) {
                    result.send({type:"user"});
                    foundInUsers = true;
                }
                deferred.resolve(foundInUsers);
            }
        });
        return deferred.promise;
    })().then(function(foundInUsers) {
        if ( foundInUsers === false ) { 
            try {
                token = JSON.parse(token).token;
            } catch (e) {}
            db.get('admins').findOne({token : token}, function(err, admin) {
                if ( err ) { errorLogger.error(err); }
                else {
                    if ( admin === undefined || admin === null ) {
                        result.send({type:"unauthorized"});
                        return;
                    }
                    else {
                        result.send({type:"admin"});
                    }
                }
            });
        }
    });
});

//ofera informatii specifice despre un user/admin in functie de request
app.get('/userinfo', function(request, result) {
    var users = db.get('users');
    var token = request.query.token;
    var single = request.query.single;
    var sports = db.get('sports');
    var arenas = db.get('arenas');
    users.findOne({token : token}, function(err, docs) {
        if ( err ) { errorLogger.error(err); }
        else
        {
            if ( docs === null || docs === undefined ) {
                var admins = db.get('admins');
                try {
                    token = JSON.parse(token).token; 
                } catch(e) {
                    token = token;
                }
                admins.findOne({token : token}, function(err, admin) {
                    if ( err ) { errorLogger.error(err); }
                    else {
                        if ( admin === null || admin === undefined ) { 
                            result.send("frate, nu e aici ce cauti tu...");
                            return;
                        }
                        if ( admin.token === '-1' ) {
                            result.send("frate, nu e aici ce cauti tu...");
                            return;
                        }
                        result.send({username : admin.username, firstName : admin.firstName});
                    }
                });
            }
            else if ( docs.token === '-1' ) {
                result.send("user_logged_out");
            }
            else
            {
                refreshAuthDate(token);
                var object = {};
                if ( single === undefined ) { //toate informatiile relevante despre un user
                    object._id = docs._id;
                    object.username = docs.username;
                    object.firstName = docs.firstName;
                    object.lastName = docs.lastName;
                    object.email = docs.email;
                    object.phoneNumber = docs.phoneNumber;
                    object.address = docs.address;
                    object.dateOfBirth = docs.dateOfBirth;
                    object.sex = docs.sex;
                    object.picture = docs.picture;
                    result.send(object);
                }
                else {
                    if ( single === 'password' ) { result.send("can't request password"); }
                    else {
                        if ( single === 'favoriteSports' ) { //sporturile favorite intr-un format simplist : { sportul : ..., este favorit : true/false }
                            var sportsArray = [];
                            var favoriteSportsArray = docs.favoriteSports;
                            sports.find({}, function(err,sports) {
                                for ( var i = 0 ; i < sports.length ; i++ ) {
                                    sportsArray[i] = {};
                                    sportsArray[i].name = sports[i].name;
                                    sportsArray[i].level = 4;
                                    sportsArray[i].isFavorite = false;
                                }
                                for ( var j = 0 ; j < sportsArray.length ; j++ )
                                {
                                    try {
                                        for ( var k = 0 ; k < favoriteSportsArray.length ; k++ ) {
                                            if ( sportsArray[j].name === favoriteSportsArray[k].name ) {
                                                sportsArray[j].isFavorite = true;
                                                sportsArray[j].level = favoriteSportsArray[k].level;
                                            }
                                        }
                                    } catch(e) {}
                                }
                                result.send(sportsArray);
                            });
                        }
                        else if ( single === 'hoursPlayed') { //cate ore a jucat si la ce anume sport (folosit la pagina de statistici)
                            var infos = [];
                            var async = require('async');
                            async.series([
                                function(callback) {
                                    sports.find({}, function(err, allSports) {
                                        if ( err ) { errorLogger.error(err); }
                                        else {
                                            if ( allSports === null || allSports === undefined || allSports.length === 0 ) { return; }
                                            for ( var i = 0 ; i < allSports.length ; i++ ) {
                                                infos[i] = {};
                                                infos[i].sport = allSports[i].name;
                                                infos[i].numberOfPlayedHours = 0;
                                            }
                                            callback(err, infos);
                                        }
                                    });
                                },
                                function(callback) {
                                    db.get('bookings').find({user : docs.username, confirmed : true}, function(err, bookingsByUser) {
                                        callback(err, bookingsByUser);
                                    });
                                },
                                function(callback) {
                                    arenas.find({}, function(err, allArenas) {
                                        callback(err, allArenas);
                                    });
                                }
                            ], function(err, response) {
                                var infos = response[0];
                                var bookingsByUser = response[1];
                                var allArenas = response[2];
                                for ( var i = 0 ; i < bookingsByUser.length ; i++ ) {
                                    var bookingDate = new Date(parseInt(bookingsByUser[i].day.split("-")[2]), parseInt(bookingsByUser[i].day.split("-")[1])-1, parseInt(bookingsByUser[i].day.split("-")[0]));
                                    var currentDate = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
                                    if ( bookingDate < currentDate || (bookingDate.toDateString() === currentDate.toDateString() && bookingsByUser[i].end < new Date().getHours())) {
                                        for ( var j = 0 ; j < allArenas.length ; j++ ) {
                                            if ( bookingsByUser[i].arenaName === allArenas[j].name ) { break; }
                                        }
                                        for ( var k = 0 ; k < infos.length ; k++ ) { 
                                            if ( allArenas[j].sportName === infos[k].sport ) {
                                                infos[k].numberOfPlayedHours += (bookingsByUser[i].end - bookingsByUser[i].start);
                                                break;
                                            }
                                        }
                                    }
                                }
                                result.send(infos);
                            });
                        }
                        else if(single === 'notifications') { //da toate notificarile necitite, iar codul comentat da ultimele 10 notificari citite sau nu
                            var unreadNotifications = [];
                            for ( var i = 0 ; i < docs.notifications.length ; i++ ) {
                                if ( docs.notifications[i].read === false ) {
                                    var noteDate = new Date(parseInt(docs.notifications[i].date.split("-")[2]), parseInt(docs.notifications[i].date.split("-")[1])-1, parseInt(docs.notifications[i].date.split("-")[0]));
                                    var currentDate = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
                                    if ( noteDate >= currentDate ) {
                                        if ( noteDate.toDateString() === currentDate.toDateString() ) {
                                            if ( docs.notifications[i].start >= new Date().getHours() + 2 ) {
                                                unreadNotifications.push(docs.notifications[i]);
                                            }
                                        }
                                        else {
                                            unreadNotifications.push(docs.notifications[i]);
                                        }
                                    }
                                }
                            }
                            result.send(unreadNotifications);
                            //decomenteaza mai jos si comenteaza mai sus daca vrei sa trimiti ultimele 10 notificari citite sau nu
                            /*var notifications = [];
                            if ( docs.notifications.length <= 10 ) {
                                result.send(docs.notifications);
                            }
                            else {
                                for ( var i = docs.notifications.length - 1 ; i >= docs.notifications.length - 11 ; i-- ) {
                                    notifications.push(docs.notifications[i]);
                                }
                                result.send(notifications);
                            }*/
                        }
                        else if (single === 'bookings') { //trimite toate booking-urile cu informatii suplimentare despre userii invitati
                            var bookings = db.get('bookings');
                            bookings.find({user : docs.username}, function(err, bookingsByUser) {
                                if ( err ) { errorLogger.error(err); }
                                else {
                                    var futureBookings = [];
                                    var async = require('async');
                                    var steps = 0;
                                    for ( var i = 0 ; i < bookingsByUser.length ; i++ ) {
                                        (function(i) {
                                            async.series([
                                                function(callback) {
                                                    var invitedUsersDetails = [];
                                                    if ( bookingsByUser[i].invitedUsers.length === 0 ) {
                                                        callback(null, []);
                                                    }
                                                    for ( var j = 0 ; j < bookingsByUser[i].invitedUsers.length ; j++ ) {
                                                        (function(j) {
                                                            (db.get('users').findOne({username : bookingsByUser[i].invitedUsers[j].user})).then(function(response) {
                                                                var auxObj = {firstName : response.firstName,
                                                                              lastName : response.lastName,
                                                                              username : response.username,
                                                                              hasAccepted : bookingsByUser[i].invitedUsers[j].hasAccepted};
                                                                invitedUsersDetails.push(auxObj);
                                                                if ( j === bookingsByUser[i].invitedUsers.length - 1 ) {
                                                                    callback(null, invitedUsersDetails);
                                                                }
                                                            });
                                                        })(j);
                                                    }
                                                },
                                                function(callback) {
                                                    db.get('arenas').findOne({name : bookingsByUser[i].arenaName}, function(err, arena) {
                                                        callback(err,arena);
                                                    });
                                                }
                                            ], function(err, response) {
                                                steps++;
                                                var bookingDate = new Date(parseInt(bookingsByUser[i].day.split("-")[2]), parseInt(bookingsByUser[i].day.split("-")[1])-1, parseInt(bookingsByUser[i].day.split("-")[0]));
                                                var currentDate = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
                                                if ( bookingDate >= currentDate ) { 
                                                    var dataToPush = bookingsByUser[i];
                                                    dataToPush.sport = response[1].sportName;
                                                    dataToPush.invitedUsers = response[0];
                                                    if ( bookingDate.toDateString() === currentDate.toDateString() && bookingsByUser[i].start > new Date().getHours() ) {
                                                        futureBookings.push(dataToPush);
                                                    }
                                                    else if ( bookingDate > currentDate ) {
                                                        futureBookings.push(dataToPush);
                                                    }
                                                }
                                                if ( steps === bookingsByUser.length ) {
                                                    result.send(futureBookings);
                                                }
                                            });
                                        })(i);
                                    }
                                }
                            });
                        }
                        else {
                            object[single] = docs[single];
                            result.send(object);
                        }
                    }
                }
            }
        }
    });
});

//codul de mai jos creeaza admini in baza de date. Inlocuieste "admin" cu userul dorit si var password = 'admin' cu parola dorita.
/*var admins = db.get('admins');
admins.findOne({username:"admin"}, function(err,docs) {
    var password = 'admin';
    crypto.pbkdf2(password, 'ala-bala-portocala', 10000, 20, function(err, derivedKey) {
        admins.update({username:"admin"}, {$set: {password:derivedKey}});
    });
});*/

//face login la admin, adica ii da acestuia un token
app.post('/admin/login', function(request, result) {
    var username = request.body.username;
    var password = request.body.password;
    var admins = db.get('admins');
    admins.findOne({username: username}, function(err, docs) {
        if ( err ) { errorLogger.error(err); }
        else {
            if(docs === null || docs === undefined) { result.status(401).end(); return; }
            crypto.pbkdf2(password, 'ala-bala-portocala', 10000, 20, function(err, derivedKey) {
                if ( err ) {
                    errorLogger.error(err);
                }
                else {
                    if(docs.password.toString() === derivedKey.toString() ) {
                        var shasum = crypto.createHash('sha1');
                        shasum.update(docs._id + Math.ceil(Math.random() * 1000000).toString(), 'utf8');
                        var hash = shasum.digest('hex');
                        admins.update({_id : docs._id}, {$set : { token : hash } }, function(err) {
                            if ( err ) { errorLogger.error(err); }
                            else { result.send({token : hash}); }
            });
                    }
                    else {
                        result.status(401).end();
                    }
                }
            });
        }
    });
});

//se face logout la admin, adica ii seteaza acestuia tokenul la valoarea "-1"
app.get('/admin/logout', function(request, result) {
    var admins = db.get('admins');
    var token = request.query.token;
    admins.update({token : token}, {$set: {token : "-1"}}, function(err) {
        if ( err ) { errorLogger.error(err); }
        else {
            result.send('success');
        }
    });
});

//da tabelul de sloturi (pe o arena anume cu bookingurile neconfirmate si informatii suplimentare despre userul care a facut bookingul) sub forma 24x7 adminului pe baza tokenului
app.get('/admin/arenas/:arenaName', function(request, result) {
    var token = request.query.token;
    try {
        token = JSON.parse(token).token;
    } catch(e) {
        errorLogger.error("JSON.parse error - " + e);
    }
    var arenaName = request.param('arenaName');
    var admins = db.get('admins');
    var bookings = db.get('bookings');
    var users = db.get('users');
    admins.findOne({token : token}, function(err, admin) {
        if ( err ) { errorLogger.error(err); }
        else {
            if ( admin === null || admin === undefined ) { result.send("Invalid token"); return; }
            if ( admin.token === '-1' ) { result.send("Admin logged out"); return; }
            var slots = [],i,j,slots2 = [];
            for ( i = 0 ; i < 24 ; i++ ) {
                slots[i] = [];
                slots2[i] = [];
                for ( j = 0 ; j < 7 ; j++ ) {
                    slots[i][j] = {
                        occupied : 0,
                        bookingID : "",
                        user : {
                            firstName : "",
                            lastName : "",
                            username : ""
                        }
                    };
                    slots2[i][j] = slots[i][j];
                }
            }
            bookings.find({arenaName : arenaName}, function(err, allBookings) {
                if ( err ) { errorLogger.error(err); }
                else {
                    if ( allBookings === null || allBookings === undefined ) { result.send([slots, slots2]); return; }
                    if ( allBookings.length === 0 ) { result.send([slots, slots2]); return; }
                    var k,steps = 0,auxSlots;
                    var async = require('async');
                    for ( k = 0 ; k < allBookings.length ; k++ ) {
                        (function(k) {
                            var a = 0; //numarul userilor care au acceptat
                            for ( var i = 0 ; i < allBookings[k].invitedUsers.length ; i++ ) {
                                if ( allBookings[k].invitedUsers[i].hasAccepted === true ) {
                                    a++;
                                }
                            }
                            console.log(allBookings[k].invitedUsers.length, a);
                            if ( allBookings[k].invitedUsers.length !== 0 ) {
                                if ( a === 0 ) {
                                    return;
                                }
                            }
                            var bookingDate = new Date(parseInt(allBookings[k].day.split("-")[2]), parseInt(allBookings[k].day.split("-")[1])-1, parseInt(allBookings[k].day.split("-")[0]));
                            var currentDate = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
                            if ( bookingDate > currentDate || ( bookingDate.toDateString() === currentDate.toDateString() && allBookings[k].start > new Date().getHours() ) ) {
                                var yearStart = new Date(new Date().getFullYear(), 0, 1);
                                var bookingWeekNo = Math.ceil((((bookingDate - yearStart) / 86400000) + yearStart.getDay())/7);
                                var currentWeekNo = Math.ceil((((currentDate - yearStart) / 86400000) + yearStart.getDay())/7);
                                if ( bookingWeekNo === currentWeekNo ) {
                                    auxSlots = slots;
                                }
                                else {
                                    auxSlots = slots2;
                                }
                                if ( allBookings[k].confirmed === false ) {
                                    async.series([
                                        function(callback) {
                                            users.findOne({username : allBookings[k].user}, function(err, user) {
                                                callback(err, user);
                                            });
                                        }
                                    ], function(err, response) {
                                        steps++;
                                        var dayOfWeek = bookingDate.getDay() - 1;
                                        if ( dayOfWeek === -1 ) { dayOfWeek = 6; }
                                        for ( var i = allBookings[k].start ; i < allBookings[k].end ; i++ ) {
                                            auxSlots[i][dayOfWeek] = {
                                                occupied : 1,
                                                bookingID : allBookings[k]._id,
                                                user : {
                                                    firstName : response[0].firstName,
                                                    lastName : response[0].lastName,
                                                    username : response[0].username
                                                }
                                            };
                                        }
                                        if ( steps === allBookings.length ) { 
                                            result.send([slots, slots2]);
                                        }
                                    });
                                }
                                else {
                                    async.series([
                                        function(callback) {
                                            users.findOne({username : allBookings[k].user}, function(err, user) {
                                                callback(err, user);
                                            });
                                        }
                                    ], function(err, response) {
                                        steps++;
                                        var dayOfWeek = bookingDate.getDay() - 1;
                                        if ( dayOfWeek === -1 ) { dayOfWeek = 6; }
                                        for ( var i = allBookings[k].start ; i < allBookings[k].end ; i++ ) {
                                            auxSlots[i][dayOfWeek] = {
                                                occupied : 2,
                                                bookingID : allBookings[k]._id,
                                                user : {
                                                    firstName : response[0].firstName,
                                                    lastName : response[0].lastName,
                                                    username : response[0].username
                                                }
                                            };
                                        }
                                        if ( steps === allBookings.length ) { 
                                            result.send([slots, slots2]);
                                        }
                                    });
                                }
                            }
                        })(k);
                    }
                }
            });
        }
    });
});

app.get('/admin/booking', function(request, result) {
    var token = request.query.token;
    var bookingID = request.query.bookingID;
    try {
        token = JSON.parse(token).token;
    } catch(e) {}
    var admins = db.get('admins');
    var bookings = db.get('bookings');
    var users = db.get('users');
    admins.findOne({token : token}, function(err, admin) {
        if ( err ) { errorLogger.error(err); }
        else {
            if ( admin === null || admin === undefined ) { result.send("Invalid token"); return; }
            if ( admin.token === '-1' ) { result.send("Admin logged out"); return; }
            bookings.findOne({_id : bookingID}, function(err, booking) {
                if ( err ) { errorLogger.error(err); }
                else {
                    if (booking === null || booking === undefined) { result.send("Booking not found!"); return; }
                    var modifiedBooking = booking;
                    var async = require('async');
                    async.series([
                        function(callback) {
                            users.findOne({username : modifiedBooking.user}, function(err, user) {
                                callback(err, user);
                            });
                        },
                        function(callback) {
                            if ( modifiedBooking.invitedUsers.length === 0 ) { 
                                callback(null, []);
                            }
                            for ( var i = 0 ; i < modifiedBooking.invitedUsers.length ; i++ ) {
                                users.findOne({username : modifiedBooking.invitedUsers[i].user}, function(err, oneUser) {
                                    callback(err, oneUser);
                                });
                            }
                        }
                    ], function(err, response) {
                        modifiedBooking.user = {
                            username : response[0].username,
                            firstName : response[0].firstName,
                            lastName : response[0].lastName
                        };
                        for (var i = 1 ; i < response.length ; i++) {
                            if ( response[i].length === 0 ) {
                                continue;
                            }
                            modifiedBooking.invitedUsers[i - 1].user = {
                                username : response[i].username,
                                firstName : response[i].firstName,
                                lastName : response[i].lastName
                            };
                        }
                        result.send(modifiedBooking);
                    });
                }
            });
        }
    });
});

//face login si trimite un token utilizatorului
app.post('/login', passport.authenticate('local'), function(request, result) {
    console.log("I received POST request for login");
    var users = db.get('users');
    users.findOne({_id:request.user._id}, function(err, docs) {
        if ( err ) { errorLogger.error(err); }
        else
        {
            if (docs === null || docs === undefined) { 
                result.send("mare_eroare_la_login");
                return;
            }
            var shasum = crypto.createHash('sha1');
            shasum.update(docs._id + Math.ceil(Math.random() * 1000000).toString(), 'utf8');
            var hash = shasum.digest('hex');
            users.update({_id:request.user._id}, {$set : { token : hash, authDate : new Date() } }, function(err) {
                if ( err ) { errorLogger.error(err); }
                else { 
                    refreshAuthDate(hash);
                    result.send({token: hash}); 
                }
            });
        }
    });
});

//updateaza poza default trimisa la pasul 1 de la signup
app.post('/signup3', function(request, result) {
    console.log("I received a POST request for signup3");
    var token = request.body.token;
    var picture = request.body.picture;
    var users = db.get('users');
    users.update({token : token}, {$set : {picture : picture}}, function(err, number) {
        if ( err ) { errorLogger.error(err); }
        else {
            if ( number === 1 ) {
                result.send("updated");
            }
        }
    });
});

//updateaza sporturile favorite si orele favorite default trimise la pasul 1 de la signup
app.post('/signup2', function(request, result) {
    console.log("I received a POST request for signup2");
    var token = request.body.token;
    var slots = request.body.slots;
    var favoriteSports = request.body.favoriteSports;
    try {
        favoriteSports = JSON.parse(favoriteSports);
    } catch(e) {}
    var users = db.get('users');
    users.update( { token: token }, {$set : { slots : slots, favoriteSports: favoriteSports } }, function(err) {
        if ( err ) { errorLogger.error(err); }
        else { result.send("ok"); }
    });
});

//face signup cu informatiile de baza ale unui user. Sporturile favorite, orele favorite si poza sunt default nule toate
app.post('/signup', function(request, result) {
    var firstName = request.body.firstName;
    var lastName = request.body.lastName;
    var username = request.body.username;
    var password = request.body.password;
    var email = request.body.email;
    var dateOfBirth = request.body.dateOfBirth;
    var sex = request.body.sex;
    var phoneNumber = request.body.phoneNumber;
    var address = request.body.address;
    var slots = request.body.slots;
    var favoriteSports = request.body.favoriteSports;
    var picture = request.body.picture;
    console.log("I received a POST request for signup");
    if ( username === '' || password === '' || email === '' || dateOfBirth === '' || firstName === '' || lastName === '' )
    {
        result.send("required_fields_empty");
        return;
    }
    try { 
        var arr = email.split('@');
        if ( arr.length === 1 ) { result.send("invalid_email"); return; }
        var arr2 = arr[1].split(".");
        if ( arr2.length === 1 ) { result.send("invalid_email"); return; }
        if ( arr[0].length < 3 && arr2[0].length < 2 || arr === null || arr.length === 0 || arr2 === null || arr2.length === 0 ) {
            result.send("invalid_email");
            return;
        }
    } catch(e) {
        result.send("invalid_email");
        return;
    }
    var collection = db.get('users');
    collection.findOne({ username: username }, function(err, user) {
        if ( err ) {
            errorLogger.error(err);
        }
        else {
            if ( !user ) {
                var array = dateOfBirth.split('-');
                if ( array === null || array === undefined) {
                    result.send("invalid_dob"); 
                    return; 
                }
                if ( array.length === 0 ) {
                    result.send("invalid_dob"); 
                    return; 
                }
                var auxDate = new Date(parseInt(array[2]), parseInt(array[1]) - 1, parseInt(array[0]));
                try {
                    if ( auxDate.getFullYear() >= 2007 || auxDate.getFullYear() <= 1890 || auxDate.getMonth() <= 0 || auxDate.getMonth() >= 13 || auxDate.getDate() <= 0 || auxDate.getDate() >= 32 || array[2].length === 2 || array[2].length === 3 || array[2].length === 1 ) {
                        result.send("invalid_dob");
                        return;
                    }
                } catch(e) { 
                    result.send("invalid_dob"); 
                    return;
                }
                if ( sex !== 'M' && sex !== 'F' ) {
                    result.send("invalid_sex"); 
                    return;
                }
                crypto.pbkdf2(password, 'ala-bala-portocala', 10000, 20, function(err, derivedKey) {
                    if ( err ) {
                        errorLogger.error(err);
                    }
                    else {
                        var deferred = Q.defer();
                        (function() {
                            var result = collection.insert({ firstName: firstName,
                                                             lastName: lastName,
                                                             username: username,
                                                             password: derivedKey,
                                                             email: email,
                                                             phoneNumber: phoneNumber,
                                                             address: address,
                                                             dateOfBirth: dateOfBirth,
                                                             sex: sex,
                                                             slots: slots,
                                                             favoriteSports: favoriteSports,
                                                             notifications: [],
                                                             picture : picture });
                            deferred.resolve(result);
                            return deferred.promise;
                        })().then(function(response) {
                            var shasum = crypto.createHash('sha1');
                            shasum.update(response.username + Math.ceil(Math.random() * 1000000).toString(), 'utf8');
                            var hash = shasum.digest('hex');
                            var users = db.get('users');
                            users.update({username : response.username}, {$set : { token : hash, authDate : new Date() } }, function(err) {
                                if ( err ) { errorLogger.error(err); }
                                else { 
                                    refreshAuthDate(hash);
                                    result.send({response : "success", token: hash}); 
                                }
                            });
                        });
                    }
                });
            }
            else {
                result.send("invalid_username");
            }
        }
    });
});

app.get('/sports', function(request, result) {
    var sports = db.get('sports');
    sports.find( {}, function(err, docs) {
        if ( err ) { errorLogger.error(err); }
        else { result.send(docs); }
    });
});

//seteaza proprietatea read a unei notificari ca fiind true atunci cand se da click pe o notificare
app.post('/readnotification', function(request, result) {
    var token = request.body.token;
    refreshAuthDate(token);
    var note = request.body.note;
    try { 
        note = JSON.parse(note);
    } catch(e) {}
    var users = db.get('users');
    users.findOne({token : token}, function(err, user) {
        if ( err ) { errorLogger.error(err); }
        else {
            if ( user === null || user === undefined ) { result.send("invalid token or user logged out"); }
            else {
                var notes = user.notifications,i;
                for ( i = 0 ; i < notes.length ; i++ ) {
                    if ( JSON.stringify(notes[i]) === JSON.stringify(note) ) {
                        notes[i].read = true;
                        users.update({token : token}, {$set:{notifications:notes}});
                        break;
                    }
                }
            }
        }
    });
    //result.status(404).end();
});

//codul de mai joc seteaza toate notificarile tuturor userilor din baza de date la array gol []
//db.get('users').update({},{$set:{notifications:[]}},{multi:true});

//accepta sau refuza o invitatie; trimite mail userului care a facut bookingul si ii face si o notificare;
app.post('/acceptinvitation', function(request, result) {
    console.log("Urmeaza sa accept o invitatie");
    var token = request.body.token;
    refreshAuthDate(token);
    var bookingID = request.body.bookingID;
    var accepted = request.body.accepted;
    var comment = request.body.comment;
    try {
        accepted = JSON.parse(accepted);
    } catch(e) {}
    try {
        token = JSON.parse(token).token;
    } catch(e) {}
    try {
        bookingID = JSON.parse(bookingID);
    } catch(e) {}
    var users = db.get('users');
    var bookings = db.get('bookings');
    (function() {
        var deferred = Q.defer();
        users.findOne({token : token}, function(err, user) {
            deferred.resolve(user);
        });
        return deferred.promise;
    })().then(function(response) {
        if ( response === undefined || response === null ) { result.send("invalid_token or user_logged_out"); }
        else {
            if ( accepted === undefined ) { 
                result.status(404).end();
                return; 
            }
            users.findOne({username : response.username}, function(err, user) {
                var notes = user.notifications;
                for ( var i = 0 ; i < notes.length ; i++ ) {
                    if ( notes[i].bookingID.toString() === bookingID ) {
                        notes[i].read = true;
                        break;
                    }
                }
                users.update({username : response.username}, {$set : {notifications : notes}});
            });
            bookings.findOne({_id : bookingID}, function(err, booking) {
                if ( err ) { errorLogger.error(err); }
                else {
                    if ( booking === undefined || booking === null ) { result.send("Booking not found"); }
                    else {
                        var mainUser = booking.user;
                        var user = response.username;
                        for ( var i = 0 ; i < booking.invitedUsers.length ; i++ ) {
                            if ( booking.invitedUsers[i].user === user ) {
                                booking.invitedUsers[i].hasAccepted = !!accepted;
                                break;
                            }
                        }
                        bookings.update({_id : bookingID}, {$set:{invitedUsers : booking.invitedUsers}});
                        users.findOne({username : mainUser}, function(err, user) {
                            if ( err ) { errorLogger.error(err); }
                            else {
                                if ( user === undefined || user === null ) { result.send("User not found"); }
                                else {
                                    var note = {
                                            type : "",
                                            bookingID : booking._id,
                                            user : "",
                                            start : booking.start,
                                            end : booking.end,
                                            date : booking.day,
                                            arena : booking.arenaName,
                                            sport : '',
                                            comment : comment,
                                            noteDate : new Date(),
                                            read : false
                                    };
                                    var async = require('async');
                                    async.parallel( [
                                        function(callback) {
                                            var arenas = db.get('arenas');
                                            arenas.findOne({name : note.arena}, function(err, arenaName) { 
                                                note.sport = arenaName.sportName;
                                                callback(err, note);
                                            });
                                        },
                                        function(callback) {
                                            db.get('users').findOne({username : response.username}, function(err, oneUser) {
                                                callback(err, {firstName : oneUser.firstName,
                                                               lastName : oneUser.lastName,
                                                               username : oneUser.username});
                                            });
                                        }
                                    ], function(err, result) {
                                        if ( !accepted ) {
                                            note.type = 'decline';
                                        }
                                        else {
                                            note.type = 'accept';
                                        }
                                        note.user = result[1];
                                        var template,title;
                                        var notifications = user.notifications;
                                        notifications.push(result[0]);
                                        users.update({username : mainUser}, {$set: {notifications : notifications}});
                                        if ( accepted === false ) {
                                            template = "Hello!\nUser " + response.username + " has declined your invitation to play " + note.sport + " on " + note.arena + ".\n";
                                            title = "Someone declined your invitation";
                                        }
                                        else {
                                            template = "Hello!\nUser " + response.username + " has accepted your invitation to play " + note.sport + " on " + note.arena + ".\n";
                                            title = "Someone accepted your invitation";
                                        }
                                        if ( result[0].comment ) {
                                            template += "His/her comment was: \n" + note.comment;
                                        }
                                        transporter.sendMail({
                                            from: 'gheorghealingabriel@gmail.com',
                                            to: user.email,
                                            subject: title,
                                            text: template
                                        }); 
                                    });
                                }
                            }
                        });
                    }
                }
            });
        }
    });
});

//trimite matricea de sloturi sub forma 24x7 cu informatii despre bookingurile facute pe o anumita arena (pe 2 saptamani : curenta si urmatoarea)
app.get('/arenas/:arenaName', function(request, result) {
    var bookings = db.get('bookings');
    var users = db.get('users');
    var token = request.query.token;
    refreshAuthDate(token);
    var occupiedThisWeek = [];
    var occupiedNextWeek = [];
    var i,j;
    for ( i = 0 ; i < 24 ; i++ ) {
        occupiedThisWeek[i] = [];
        occupiedNextWeek[i] = [];
        for ( j = 0 ; j < 7 ; j++ ) {
            occupiedThisWeek[i][j] = 0;
            occupiedNextWeek[i][j] = 0;
        }
    }
    var found = false;
    var day = new Date().getDay() - 1;
    if ( day === -1 ) { day = 6; }
    for ( i = 0 ; i < 7 ; i++ ) {
        for ( j = 0 ; j < 24 ; j++ ) { 
            if ( i === day && j === new Date().getHours() + 1 ) {
                found = true;
                break;
            }
            occupiedThisWeek[j][i] = -1;
        }
        if ( found ) {
            break;
        }
    }
    users.findOne({token : token}, function(err, user) {
        if ( err ) { errorLogger.error(err); return; }
        else {
            if ( user === undefined || user === null || user.token === '-1' ) { result.send("invalid_token or user_logged_out"); }
            else {
                var allowed = 2;
                var async = require('async');
                async.series([ 
                    function(callback) {
                        (bookings.find({user : user.username})).then(function(bookingsByUser) {
                            var arenas = db.get('arenas');
                            async.series([
                                function(callback) {
                                    arenas.findOne({name : request.param("arenaName")}, function(err, arena) {
                                        arenas.find({sportName : arena.sportName}, function(err, allArenasBySport) {
                                            callback(err, allArenasBySport);
                                        });
                                    });
                                }
                            ], function(err, response) {
                                var allArenasBySport = response[0];
                                var bookingDate;
                                for ( var index = 0 ; index < bookingsByUser.length ; index++ ) {
                                    for ( var k = 0 ; k < allArenasBySport.length ; k++ ) {
                                        if ( allArenasBySport[k].name === bookingsByUser[index].arenaName ) {
                                            break;
                                        }
                                    }
                                    if ( k < allArenasBySport.length ) {
                                        bookingDate = new Date(parseInt(bookingsByUser[index].day.split("-")[2]), parseInt(bookingsByUser[index].day.split("-")[1])-1, parseInt(bookingsByUser[index].day.split("-")[0]));
                                        var currentDate2 = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
                                        if ((bookingDate - currentDate2)/1000/60/60/24 < 7) {
                                            if (bookingDate >= currentDate2) {
                                                allowed--;
                                                if ( allowed === 0 ) {
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                                callback(null, allowed);
                            });
                        });
                    },
                    function(callback) {
                        bookings.find({arenaName : request.param("arenaName")}, function(err, bookings) {
                            if ( err ) { errorLogger.error(err); }
                            else {
                                for ( var i = 0 ; i < bookings.length ; i++ ) {
                                    var bookingDate = new Date(parseInt(bookings[i].day.split("-")[2]), parseInt(bookings[i].day.split("-")[1])-1, parseInt(bookings[i].day.split("-")[0]));
                                    var currentDate = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
                                    var j;
                                    if (bookingDate >= currentDate) {
                                        var dayOfWeek = bookingDate.getDay();
                                        if ( dayOfWeek === 0 ) { dayOfWeek = 6; }
                                        else { dayOfWeek--; }
                                        var yearStart = new Date(new Date().getFullYear(),0,1);
                                        var bookingWeekNo = Math.ceil((((bookingDate - yearStart) / 86400000) + yearStart.getDay())/7);
                                        var currentWeekNo = Math.ceil((((currentDate - yearStart) / 86400000) + yearStart.getDay())/7);
                                        if (bookingWeekNo === currentWeekNo) {
                                            if (bookings[i].confirmed === true && user.username !== bookings[i].user) {
                                                for ( j = bookings[i].start ; j < bookings[i].end ; j++ ) {
                                                    occupiedThisWeek[j][dayOfWeek] = 1;
                                                }
                                            }
                                            if (bookings[i].confirmed === true && user.username === bookings[i].user) {
                                                for ( j = bookings[i].start ; j < bookings[i].end ; j++ ) {
                                                    occupiedThisWeek[j][dayOfWeek] = 2;
                                                }
                                            }
                                            if (bookings[i].confirmed === false && user.username === bookings[i].user) {
                                                for ( j = bookings[i].start ; j < bookings[i].end ; j++ ) {
                                                    occupiedThisWeek[j][dayOfWeek] = 3;
                                                }
                                            }
                                            if (bookings[i].confirmed === false && user.username !== bookings[i].user) {
                                                for ( j = bookings[i].start ; j < bookings[i].end ; j++ ) {
                                                    occupiedThisWeek[j][dayOfWeek] = 1;
                                                }
                                            }
                                        }
                                        else if (bookingWeekNo === currentWeekNo + 1) {
                                            if (bookings[i].confirmed === true && user.username !== bookings[i].user) {
                                                for ( j = bookings[i].start ; j < bookings[i].end ; j++ ) {
                                                    occupiedNextWeek[j][dayOfWeek] = 1;
                                                }
                                            }
                                            if (bookings[i].confirmed === true && user.username === bookings[i].user) {
                                                for ( j = bookings[i].start ; j < bookings[i].end ; j++ ) {
                                                    occupiedNextWeek[j][dayOfWeek] = 2;
                                                }
                                            }
                                            if (bookings[i].confirmed === false && user.username === bookings[i].user) {
                                                for ( j = bookings[i].start ; j < bookings[i].end ; j++ ) {
                                                    occupiedNextWeek[j][dayOfWeek] = 3;
                                                }
                                            }
                                            if (bookings[i].confirmed === false && user.username !== bookings[i].user) {
                                                for ( j = bookings[i].start ; j < bookings[i].end ; j++ ) {
                                                    occupiedNextWeek[j][dayOfWeek] = 1;
                                                }
                                            }
                                        }
                                    }
                                }
                                callback(null, [occupiedThisWeek, occupiedNextWeek]);
                            }
                        });
                    }], function(err, response) {
                            errorLogger.error(err);
                            result.send({schedule : response[1],
                                         allowed : response[0]});
                });
            }
        }
    });
});

//verifica daca username-ul exista deja in baza de date
app.post('/checkusername', function(request, result) {
    var username = request.body.username;
    if ( username === '' ) {
        result.send(false);
        return;
    }
    var users = db.get('users');
    users.find( { username : username }, function(err, docs) {
        if ( err ) { errorLogger.error(err); }
        else
        {
            if ( docs === null || docs.length === 0 ) { result.send(true); }
            else { result.send(false); }
        }
    });
});

app.get('/arenas', function(request, result) {
    var token = request.query.token;
    refreshAuthDate(token);
    var sport = request.query.sport;
    var users = db.get('users');
    users.findOne({token:token}, function(err,docs) {
        if ( err ) { errorLogger.error(err); }
        else {
            if ( docs === null ) { result.send("token_error or user_logged_out"); return; }
            else {
                var arenas = db.get("arenas");
                if ( sport === undefined ) {
                    arenas.find({}, function(err, arenas) {
                        var sports = db.get("sports");
                        sports.find( {}, function(err, docs) {
                            if ( err ) { errorLogger.error(err); }
                            else {
                                var i;
                                var obj = {};
                                for ( i = 0 ; i < docs.length ; i++ ) {
                                    obj[docs[i].name] = [];
                                }
                                for ( i = 0 ; i < arenas.length ; i++ ) {
                                    obj[arenas[i].sportName].push(arenas[i]);
                                }
                                result.send(obj);
                            }
                        });
                    });
                }
                else
                {
                    arenas.find({sportName:sport}, function(err, arenas) {
                        if ( err ) { errorLogger.error(err); }
                        else {
                            if ( arenas === null || arenas.length === 0 ) { result.send("invalid_sport"); }
                            else { result.send(arenas); }
                        }
                    });
                }
            }
        }
    }); 
});

//ii trimite adminului informatii despre booking-urile neconfirmate
app.get('/booking', function(request, result) {
    var admins = db.get('admins');
    var token = request.query.token;
    try {
        token = JSON.parse(token).token;
    } catch(e) {}
    refreshAuthDate(token);
    admins.findOne({token : token}, function(err, admin) {
        if ( err ) { errorLogger.error(err); return; }
        else {
            if ( admin === null || admin === undefined ) { result.send("invalid_token or admin_logged_out"); return; }
            if ( admin.token === '-1' ) { result.send("invalid_token or admin_logged_out"); return; }
            var bookings = db.get('bookings');
            bookings.find({confirmed : false}, function(err,unconfirmedBookings) {
                if ( err ) { errorLogger.error(err); }
                else {
                    var unconfBookings = [];
                    for ( var i = 0 ; i < unconfirmedBookings.length ; i++ ) {
                        var bookingDate = new Date(parseInt(unconfirmedBookings[i].day.split("-")[2]), parseInt(unconfirmedBookings[i].day.split("-")[1])-1, parseInt(unconfirmedBookings[i].day.split("-")[0]));
                        var currentDate = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
                        if ( bookingDate >= currentDate ) {
                            for ( var j = 0 ; j < unconfirmedBookings[i].invitedUsers.length ; j++ ) {
                                if ( unconfirmedBookings[i].invitedUsers[j].hasAccepted === true ) {
                                    break;
                                }
                            }
                            if ( j !== unconfirmedBookings[i].invitedUsers.length || unconfirmedBookings[i].invitedUsers.length === 0 ) {
                                unconfBookings.push(unconfirmedBookings[i]);
                                bookingDate.setMonth(bookingDate.getMonth() + 1);
                                unconfBookings[unconfBookings.length - 1].day = bookingDate.getDate() + "-" + bookingDate.getMonth() + "-" + bookingDate.getFullYear();
                            }
                        }
                    }
                    result.send(unconfBookings);
                }
            });
        }
    });
});

//se face cancel la un booking; face notificari de tip cancel tuturor userilor invitati
app.post("/booking/cancel", function(request, result) {
    var bookings = db.get("bookings");
    var bookingID = request.body.bookingID;
    var token = request.body.token;
    refreshAuthDate(token);
    db.get('users').findOne({token : token}, function(err, oneUser) {
        if ( err ) { errorLogger.error(err); }
        else {
            if ( oneUser === null || oneUser === undefined ) { result.send("invalid token"); return; }
            if ( oneUser.token === '-1' ) { result.send("user logged out"); return; }
            bookings.findOne({_id : bookingID}, function(err, booking) {
                if ( err ) { errorLogger.error(err); }
                else {
                    if ( booking === null || booking === undefined ) { return; }
                    var bookingDate = new Date(parseInt(booking.day.split("-")[2]), parseInt(booking.day.split("-")[1])-1, parseInt(booking.day.split("-")[0]));
                    var currentDate = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
                    if ((bookingDate - currentDate) / 1000 / 60 / 60 < 24 ) {
                        result.send("Can't cancel booking if it's happening in less than 24 hours");
                        return;
                    }
                    else {
                        bookings.remove({_id : bookingID});
                        result.send("Booking canceled!");
                    }
                    for ( var i = 0 ; i < booking.invitedUsers.length ; i++ ) {
                        (function(i) {
                            var note = {
                                    type : "cancel",
                                    user : oneUser.username,
                                    bookingID : booking._id,
                                    start : booking.start,
                                    end : booking.end,
                                    date : booking.day,
                                    arena : booking.arenaName,
                                    sport : '',
                                    comment : "",
                                    noteDate : new Date(),
                                    read : false
                            };
                            var async = require('async');
                            async.series([
                                function(callback) {
                                    db.get('arenas').findOne({name : booking.arenaName}, function(err, arena) {
                                        callback(err, arena.sportName);
                                    });
                                },
                                function(callback) {
                                    db.get('users').findOne({username : oneUser.username}, function(err, user) {
                                        callback(err, {firstName : user.firstName,
                                                       lastName : user.lastName,
                                                       username : user.username});
                                    });
                                },
                                function(callback) {
                                    db.get('users').findOne({username : booking.invitedUsers[i].user}, function(err, invitedUser) {
                                        callback(err,invitedUser.notifications);
                                    });
                                }
                            ], function(err, response) {
                                note.sport = response[0];
                                note.user = response[1];
                                var notifications = response[2];
                                for ( var j = 0 ; j < notifications.length ; j++ ) {
                                    if ( notifications[i].bookingID === bookingID ) { 
                                        notifications.splice(j,1);
                                    }
                                }
                                notifications.push(note);
                                db.get('users').update({username : booking.invitedUsers[i].user}, {$set : {notifications : notifications}});
                            });
                        })(i);
                    }
                }
            });
        }
    });
});

//asta e toata magia. Aici se introduce booking-ul in baza de date
//tot ruta asta se foloseste si atunci cand un admin confirma booking-ul sau ii da decline; cred ca ar trebui schimbat asta...
app.post('/booking', function(request, result) {
    var confirmBooking = request.body.confirm;
    var bookings = db.get("bookings");
    var bookingID = request.body.bookingID;
    var token = request.body.token;
    var users = db.get('users');
    if ( confirmBooking === undefined ) {
        var start,end,day;
        refreshAuthDate(token);
        var arenaName = request.body.arenaName;
        var slots = request.body.slots;
        var week = request.body.week;
        try {
            slots = JSON.parse(slots);
            for ( var m = 0 ; m < slots.length ; m++ ) {
                slots[m] = JSON.parse(slots[m]);
            }
        } catch(e) {}
        var invitedUsers = request.body.invitedUsers;
        try {
            invitedUsers = JSON.parse(invitedUsers);
        } catch(e) {}
        var found = false;
        for ( var j = 0 ; j < 7 ; j++ ) {
            for ( var i = 0 ; i < 24 ; i++ ) {
                if ( slots[i][j] === true ) {
                    start = i;
                    //i++;
                    while ( i <= 23 && slots[i][j] === true ) {
                        i++;
                    }
                    end = i;
                    day = j;
                    found = true;
                    break;
                }
            }
            if ( found === true ) {
                break;
            }
        }
        if ( end - start > 2 ) {
            result.send("You can't book more than 2 hours");
            return;
        }
        users.findOne({token : token}, function(err, user) {
            if ( err ) { errorLogger.error(err); return; }
            else {
                if ( user === null || user === undefined || user.token === '-1' ) { result.send("invalid_token or user_logged_out"); }
                else {
                    var bookings = db.get('bookings');
                    (bookings.find({user : user.username})).then(function(bookingsByUser) {
                        var async = require('async');
                        var arenas = db.get('arenas');
                        var allowed = true;
                        async.series([
                            function(callback) {
                                arenas.findOne({name : arenaName}, function(err, arena) {
                                    arenas.find({sportName : arena.sportName}, function(err, allArenasBySport) {
                                        callback(err, allArenasBySport);
                                    });
                                });
                            }
                        ], function(err, response) {
                            var allArenasBySport = response[0];
                            var count = 0,bookingDate;
                            for ( var index = 0 ; index < bookingsByUser.length ; index++ ) {
                                for ( var k = 0 ; k < allArenasBySport.length ; k++ ) {
                                    if ( allArenasBySport[k].name === bookingsByUser[index].arenaName ) {
                                        break;
                                    }
                                }
                                if ( k < allArenasBySport.length ) {
                                    bookingDate = new Date(parseInt(bookingsByUser[index].day.split("-")[2]), parseInt(bookingsByUser[index].day.split("-")[1])-1, parseInt(bookingsByUser[index].day.split("-")[0]));
                                    var currentDate2 = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
                                    if (bookingDate >= currentDate2) {
                                        count++;
                                        if ( count >= 2 ) {
                                            allowed = false;
                                            break;
                                        }
                                    }
                                }
                            }
                            if ( allowed === false ) {
                                result.send("You can't book more than 2 times a week on " + arenaName);
                                return;
                            }
                            var currentDay = new Date().getDay();
                            bookingDate = new Date();
                            var nextWeek = parseInt(week) * 7;
                            nextWeek = nextWeek || 0;
                            bookingDate.setDate(bookingDate.getDate() + day - currentDay + 1 + nextWeek);
                            bookingDate.setMonth(bookingDate.getMonth() + 1);
                            day = bookingDate.getDate() + '-' + bookingDate.getMonth() + '-' + bookingDate.getFullYear();
                            bookingDate.setMonth(bookingDate.getMonth() - 1);
                            var currentDate = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
                            if (bookingDate >= currentDate) {
                                if ( (bookingDate === currentDate && start >= new Date().getHours()) || (bookingDate > currentDate) ) {
                                    if ( invitedUsers === undefined || invitedUsers === null || invitedUsers.length === 0 ) {
                                        invitedUsers = [];
                                    }
                                    else {
                                        var invitedUsers2 = [];
                                        for ( var i = 0 ; i < invitedUsers.length ; i++ ) {
                                            invitedUsers2[i] = {};
                                            invitedUsers2[i].user = invitedUsers[i];
                                            invitedUsers2[i].hasAccepted = false;
                                        }
                                        invitedUsers = invitedUsers2;
                                    }
                                    (bookings.insert({arenaName : arenaName,
                                                      user : user.username,
                                                      day : day,
                                                      start : start,
                                                      end : end,
                                                      invitedUsers: invitedUsers,
                                                      confirmed : false}))
                                        .then(function(response) {
                                            for ( var j = 0 ; j < invitedUsers.length ; j++ ) {
                                                (function(j) {
                                                    var users = db.get('users');
                                                    users.findOne({username : invitedUsers[j].user}, function(err, invitedUser) {
                                                        if ( err ) { errorLogger.error(err); return; }
                                                        if ( invitedUser === null || invitedUser === undefined ) { result.send("Cannot find user"); return; }
                                                        var template = "Hello!\nYou have been invited by " + user.firstName + " " + user.lastName + " to play on " + arenaName;
                                                        var invitedUsersWithoutSelf = [];
                                                        if ( invitedUsers.length >= 2 ) {
                                                            template += " with him and ";
                                                            for ( var k = 0 ; k < invitedUsers.length ; k++ ) {
                                                                if ( invitedUsers[k].user !== invitedUser.username ) {
                                                                    template += invitedUser.user + " ";
                                                                    invitedUsersWithoutSelf.push(invitedUsers[k]);
                                                                }
                                                            }
                                                        }
                                                        template += "\b.\nIf you want to accept the invitation please enter the website.\n\nhttp://192.168.1.12:5555/#/home";
                                                        if ( invitedUser.notifications === null || invitedUser.notifications === undefined ) {
                                                            invitedUser.notifications = [];
                                                        }
                                                        var arenas = db.get('arenas');
                                                        (arenas.findOne({name : response.arenaName})).then(function(arena) {
                                                            invitedUser.notifications.push({bookingID : response._id,
                                                                                            type : "invite",
                                                                                            invitedBy : {firstName : user.firstName,
                                                                                                         lastName : user.lastName,
                                                                                                         username : user.username},
                                                                                            invitedUsers : invitedUsersWithoutSelf,
                                                                                            start : response.start,
                                                                                            end : response.end,
                                                                                            date : response.day,
                                                                                            arena : response.arenaName,
                                                                                            sport : arena.sportName,
                                                                                            noteDate : new Date(),
                                                                                            read: false});
                                                            users.update({username : invitedUser.username}, {$set: {notifications : invitedUser.notifications}}, function(err, updatedUsers) {
                                                                if ( err ) { errorLogger.error(err); }
                                                                else {
                                                                    if ( updatedUsers === 1 ) {
                                                                        transporter.sendMail({
                                                                            from: 'gheorghealingabriel@gmail.com',
                                                                            to: invitedUser.email,
                                                                            subject: 'Invitation',
                                                                            text: template
                                                                        }); 
                                                                    }
                                                                }
                                                            });
                                                        });
                                                    });   
                                                })(j);
                                            }
                                        result.send("Succes! Booking waiting to be confirmed");
                                    });

                                }
                            }
                            else {
                                result.send("Invalid date");
                            }
                        });
                        
                    });
                }
            }
        });
    }
    else {
        confirmBooking = JSON.parse(confirmBooking);
        if ( confirmBooking === true ) {
            bookings.update({_id : bookingID}, {$set : { confirmed : true }}, function(err, docs) {
                if ( err ) { errorLogger.error(err); }
                else { 
                    if ( docs === 0 ) { result.send("There was an error in confirming this booking"); }
                    else {
                        bookings.findOne({_id : bookingID}, function(err, booking) {
                            users.findOne({username : booking.user}, function(err, user) {
                                if ( err ) { errorLogger.error(err); }
                                else {
                                    if ( user === null || user === undefined ) { result.send("Eroare"); }
                                    else {
                                        var note = {
                                                type : "accept",
                                                user : {firstName : "admin", lastName : "admin", username : "admin" },
                                                bookingID : booking._id,
                                                start : booking.start,
                                                end : booking.end,
                                                date : booking.day,
                                                arena : booking.arenaName,
                                                sport : '',
                                                comment : "",
                                                noteDate : new Date(),
                                                read : false
                                        };
                                        var async = require('async');
                                        async.series([
                                            function(callback) {
                                                var arenas = db.get('arenas');
                                                arenas.findOne({name : note.arena}, function(err, arenaName) { 
                                                    note.sport = arenaName.sportName;
                                                    callback(err, note);
                                                });
                                            }
                                        ], function(err, result) {
                                            var notifications = user.notifications;
                                            notifications.push(result[0]);
                                            users.update({username : user.username}, {$set: {notifications : notifications}}, function(err, docs) {
                                                if ( err ) { errorLogger.error(err); }
                                                else {
                                                    if ( docs === 1 ) {
                                                        transporter.sendMail({
                                                            from: 'gheorghealingabriel@gmail.com',
                                                            to: user.email,
                                                            subject: 'Booking confirmed',
                                                            text: 'Hello!\nYour booking from ' + booking.start + " to " + booking.end + " on " + booking.day + " was been successfully confirmed.\nThank you for choosing our services!"
                                                        });
                                                    }
                                                }
                                            });
                                        });
                                    }
                                }
                            });
                        });
                        result.send("Booking confirmed! Have fun!"); 
                    }
                }
            });
        }
    }
});

//face logout; seteaza tokenul la valoarea "-1"
app.get('/logout', function(request, result) {
    console.log("I received GET for logout");
    var token = request.query.token;
    var users = db.get('users');
    users.update({token : token}, {$set : { token : "-1" }}, function(err, number) {
        if ( err ) { errorLogger.error(err); }
        else { 
            if ( number === 0 ) {
                var admins = db.get('admins');
                admins.update({token : token}, {$set : {token : "-1" }}, function(err, number) {
                    if ( err ) { errorLogger.error(err); }
                    else {
                        if ( number === 0 ) {
                            result.send('cant_logout');
                        }
                        else {
                            result.send("admin_logout_successful");
                        }
                    }
                });
            }
            else {
                result.send("user_logout_successful");
            }
        }
    });
});

//daca sports2 se gaseste in sports1, returneaza pozitia la care se gaseste acesta in vectorul sports1
function intersectSports(sports1, sports2) {
    for(var i = 0 ; i < sports1.length ; i++ ) {
        for(var j = 0 ; j < sports2.length ; j++ ) {
            if ( sports1[i].name === sports2[j] ) {
                return i;
            }
        }
    }
    return -1;
}

//transpune o matrice 7x24 intr-una 24x7
function transposeMatrix(matrix) {
    var auxMatrix = [],i,j;
    for ( i = 0 ; i < 7 ; i++ ) {
        auxMatrix[i] = [];
        for ( j = 0 ; j < 24 ; j++ ) {
            auxMatrix[i][j] = 0;
        }
    }
    for ( i = 0 ; i < 24 ; i++ ) {
        for ( j = 0 ; j < 7 ; j++ ) {
            auxMatrix[j][i] = matrix[i][j];
        }
    }
    return auxMatrix;
}

//intersecteaza orele favorite ale unui user cu ale altuia
//se foloseste doar ca sa zica daca orele favorite ale unui user se intersecteaza cu booking-ul curent
function intersectSlots(slots1, slots2) {
    var i,j,relevant = 0,hours = 0;
    for ( i = 0 ; i < 7 ; i++ ) {
        for ( j = 0 ; j < 24 ; j++ ) {
            slots1[i][j] = !!slots1[i][j];
            slots2[i][j] = !!slots2[i][j];
        }
    }
    for ( i = 0 ; i < 7 ; i++ ) {
        for ( j = 0 ; j < 24 ; j++ ) {
            if ( slots2[i][j] === true ) {
                hours++;
            }
        }
    }
    for ( i = 0 ; i < 7 ; i++ ) {
        for ( j = 0 ; j < 24 ; j++ ) {
            while ( slots1[i][j] === slots2[i][j] && slots1[i][j] === true ) {
                relevant++;
                j++;
                if ( j === 24 ) {
                    break; 
                }
            }
        }
    }
    return (relevant >= hours);
}

//trimite unui user informatii despre toti ceilalti useri care sunt compatibili cu el
//compatibilitate inseamna : orele selectate de user sunt printre orele favorite ale unui user si sportul la care userul vrea sa faca booking-ul se afla printre sporturile favorite ale unui user
app.post('/users', function(request, result) {
    var token = request.body.token;
    refreshAuthDate(token);
    var slots = request.body.slots;
    var arenaName = request.body.arenaName;
    var i,j,found=false;
    try {
        slots = JSON.parse(slots);
        for ( i = 0 ; i < 24 ; i++ ) {
            slots[i] = !!(JSON.parse(slots[i]));
        }
    } catch(e) {}
    var auxSlots = [];
    for ( i = 0 ; i < 24 ; i++ ) {
        auxSlots[i] = [];
        for ( j = 0 ; j < 7 ; j++ ) {
            auxSlots[i][j] = 0;
        }
    }
    for ( j = 0 ; j < 7 ; j++ ) {
        for ( i = 0 ; i < 24 ; i++ ) {
            if ( slots[i][j] === true ) {
                auxSlots[i][j] = 1;
                while ( i <= 23 && slots[i][j] === true ) {
                    auxSlots[i][j] = 1;
                    i++;
                }
                found = true;
                break;
            }
        }
        if ( found === true ) {
            break;
        }
    }
    var arenas = db.get('arenas');
    var sportName;
    (arenas.findOne({name:arenaName})).then(function(response) {
        sportName = response.sportName;
    });
    var users = db.get("users");
    var usersArray = [];
    users.findOne({token : token}, function(err, user) {
        if(err) { errorLogger.error(err); }
        else {
            if ( user === null ) { 
                result.send("token_error or user_logged_out");
                return; 
            }
            users.find({}, function(err,allUsers) {
                if(err) { errorLogger.error(err); }
                else {
                    for(var i = 0 ; i < allUsers.length ; i++) {
                        if(allUsers[i].username === user.username) { continue; }
                        var sportIntersect = 0;
                        try {
                            sportIntersect = intersectSports(allUsers[i].favoriteSports, [sportName]);
                        } catch(e) {}
                        if ( sportIntersect !== -1 ) {
                            var slotsIntersect = intersectSlots(allUsers[i].slots, transposeMatrix(auxSlots));
                            if ( slotsIntersect ) {
                                usersArray.push({firstName: allUsers[i].firstName,
                                                 lastName : allUsers[i].lastName,
                                                 username : allUsers[i].username,
                                                 email : allUsers[i].email,
                                                 phoneNumber : allUsers[i].phoneNumber,
                                                 address : allUsers[i].address,
                                                 picture : allUsers[i].picture,
                                                 skill : allUsers[i].favoriteSports[sportIntersect].level
                                                });
                            }
                        }
                    }
                    result.send(usersArray);
                }
            });
        }
    });
});

//trimite sub binecunoscuta pana acum forma matriceala 24x7 vremea pe 2 saptamani (curenta si urmatoarea)
/*      WEATHER       */
app.get("/weather", function(request, result) {
    var token = request.query.token;
    var req = require('request');
    var async = require('async');
    refreshAuthDate(token);
    var forecastThisWeek = [];
    var forecastNextWeek = [];
    for ( var i = 0 ; i < 24 ; i++ ) {
        forecastThisWeek[i] = [];
        forecastNextWeek[i] = [];
        for ( var j = 0 ; j < 7 ; j++ ) {
            forecastThisWeek[i][j] = null;
            forecastNextWeek[i][j] = null;
        }
    }
    var users = db.get('users');
    users.findOne({token:token}, function(err, user) {
        if ( err ) { errorLogger.error(err); }
        else {
            if ( user === undefined || user === null ) { result.send("invalid_token or user_logged_out"); return; }
            if ( user.token === '-1' ) { result.send("user_logged_out"); }
            else {
                async.series([
                    function(callback) {
                        var pathFor2Weeks = "http://api.openweathermap.org/data/2.5/forecast/daily?lat=44.452433&lon=26.086032&cnt=14&mode=json";
                        req(pathFor2Weeks, function(error, response, body) {
                            if ( !error && response.statusCode === 200 ) {
                                var weatherObj;
                                try {
                                    weatherObj = JSON.parse(body);
                                } catch(e) {
                                    errorLogger.error("JSON.parse error " + e);
                                    weatherObj = body;
                                }
                                var date = new Date(weatherObj.list[0].dt * 1000);
                                var day = date.getDay() - 1;
                                if ( day === -1 ) { day = 6; }
                                for ( var k = 0 ; k < 7 ; k++ ) {
                                    for ( var i = 0 ; i < 24 ; i++ ) {
                                        forecastNextWeek[i][k] = {
                                            temp : weatherObj.list[k + 7 - day].temp.day - 273.15,
                                            humidity : weatherObj.list[k + 7 - day].humidity,
                                            weather : {
                                                main : weatherObj.list[k + 7 - day].weather[0].main,
                                                description : weatherObj.list[k + 7 - day].weather[0].description,
                                                icon : weatherObj.list[k + 7 - day].weather[0].icon
                                            }
                                        };
                                        forecastThisWeek[i][k + day] = {
                                            temp : weatherObj.list[k + day].temp.day - 273.15,
                                            humidity : weatherObj.list[k + day].humidity,
                                            weather : {
                                                main : weatherObj.list[k + day].weather[0].main,
                                                description : weatherObj.list[k + day].weather[0].description,
                                                icon : weatherObj.list[k + day].weather[0].icon
                                            }
                                        };
                                    }
                                }
                                callback(null, forecastNextWeek);
                            }
                        });
                    },
                    function(callback) {
                        var pathFor5Days = "http://api.openweathermap.org/data/2.5/forecast?lat=44.452433&lon=26.086032&mode=json";
                        req(pathFor5Days, function(error, response, body) {
                            if ( !error && response.statusCode === 200 ) {
                                var weatherObj = JSON.parse(body);
                                var prevDay = -1,day;
                                for ( var k = 0 ; k < weatherObj.list.length ; k++ ) {
                                    var date = new Date(weatherObj.list[k].dt * 1000);
                                    day = date.getDay() - 1;
                                    if ( day === -1 ) { day = 6; }
                                    if ( prevDay === 6 && day === 0 ) {
                                        break;
                                    }
                                    var hour = parseInt(date.toString().split(" ")[4].split(":")[0]);
                                    forecastThisWeek[hour][day] = {
                                        temp : weatherObj.list[k].main.temp - 273.15,
                                        humidity : weatherObj.list[k].main.humidity,
                                        weather : {
                                            main : weatherObj.list[k].weather[0].main,
                                            description : weatherObj.list[k].weather[0].description,
                                            icon : weatherObj.list[k].weather[0].icon
                                        },
                                        wind : {
                                            speed : weatherObj.list[k].wind.speed,
                                            deg : weatherObj.list[k].wind.deg
                                        }
                                    };
                                    forecastThisWeek[hour+1][day] = forecastThisWeek[hour][day];
                                    forecastThisWeek[hour+2][day] = forecastThisWeek[hour][day];
                                    prevDay = day;
                                }
                                day = new Date().getDay() - 1;
                                if ( day === -1 ) { day = 6; }
                                var found = false;
                                for ( var i = 0 ; i < 24 ; i++ ) {
                                    for ( var j = 0 ; j <= day ; j++ ) {
                                        if ( j === day && i === new Date().getHours() ) {
                                            found = true;
                                            break;
                                        }
                                        forecastThisWeek[i][j] = null;
                                    }
                                    if ( found === true ) {
                                        break;
                                    }
                                }
                                callback(null, forecastThisWeek);
                            }
                        });
                    }
                ], function(err, forecast) {
                    if ( err ) { errorLogger.error(err); }
                    var aux = forecast[0];
                    forecast[0] = forecast[1];
                    forecast[1] = aux;
                    result.send(forecast);
                });
            }
        }
    });
});

//trimite adminului un raport cu statistici despre useri : cate ore au jucat, cate bookinguri au fost confirmate, cate bookinguri au fost neconfirmate
app.get('/report', function(request, result) {
    var token = request.query.token;
    var admins = db.get('admins');
    var users = db.get('users');
    var bookings = db.get('bookings');
    try {
        token = JSON.parse(token).token;
    } catch(e) {}
    admins.findOne({token : token}, function(err, admin) {
        if ( err ) { errorLogger.error(err); }
        else {
            if ( admin === null || admin === undefined ) { result.send("admin_logged_out or invalid_token"); return; }
            if ( admin.token === '-1' ) { result.send("admin_logged_out"); }
            else {
                var Q = require('q');
                var deferred = Q.defer();
                (function() {
                    users.find({}, function(err, allUsers) {
                        deferred.resolve(allUsers);
                    });
                    return deferred.promise;
                })().then(function(response) {    
                    var report = [],j,steps = 0;
                    for ( j = 0 ; j < response.length ; j++ ) {
                        (function(j) { 
                            var name = {
                                firstName : response[j].firstName,
                                lastName : response[j].lastName,
                                username : response[j].username };
                            (bookings.find({user : response[j].username})).then(function(data) {
                                var auxObject = {};
                                auxObject.name = name;
                                auxObject.numberOfConfirmedBookings = 0;
                                auxObject.numberOfUnconfirmedBookings = 0;
                                auxObject.numberOfPlayedHours = 0;
                                report.push(auxObject);
                                if ( data === null || data === undefined || data.length === 0 ) { 
                                    steps++; 
                                    if ( steps === response.length - 1 ) { 
                                        result.send(report);
                                    }
                                    return; 
                                }
                                for ( var i = 0 ; i < data.length ; i++ ) {
                                    var bookingDate = new Date(parseInt(data[i].day.split("-")[2]), parseInt(data[i].day.split("-")[1])-1, parseInt(data[i].day.split("-")[0]));
                                    var currentDate = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
                                    if ( bookingDate < currentDate ) {
                                        if ( data[i].confirmed === true ) {
                                            auxObject.numberOfConfirmedBookings++;
                                            auxObject.numberOfPlayedHours += (data[i].end - data[i].start);
                                        }
                                        else {
                                            auxObject.numberOfUnconfirmedBookings++;
                                        }
                                    }
                                }
                                steps++;
                                if ( steps === response.length - 1 ) { 
                                    result.send(report);
                                }
                            });
                        })(j);
                    }
                });
            }
        }
    });
});

//fara liniile astea 3, nimic nu s-ar intampla...
//porneste serverul
http.createServer(app).listen(app.get('port'), function(){
  console.log('Server listening on port ' + app.get('port'));
});