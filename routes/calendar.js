var express = require('express');
var router = express.Router();
var monk = require('monk');
var mongoUri = 'localhost:27017/eeStecDB';
var db = monk(mongoUri);

router.get('/', function(request, result) {
  	var date = request.query.date;
  	var token = request.query.token;
  	var users = db.get("users");
  	users.findOne({token : token}, function(err, user) {
		if (err) {
			result.send(err);
			return ;
		}
		if (user === null || user === undefined) {
			result.send("User is null");
			return;
		}
		if (user.token === "-1") {
			result.send("User not logged");
			return;
		}
		else {
			//result.send(user.calendar[date]);
            if (user.calendar[date] === undefined) {
                var array = [];
                for (var i = 0; i < 48; i++) {
                    array[i] = {
                        isOccupied : false
                    };
                }
                result.send(array);
            }
            else {
                result.send(user.calendar[date]);
            }
			return;
		}	
	});
});

router.post('/', function(request, result)
{
	var date = request.body.date;
	var token = request.body.token;
	var schedule = request.body.schedule;
	var users = db.get("users");
	users.findOne({token : token}, function(err, user)
	{
		if (err)
		{
			result.send(err);
			return;
		}
		else if (user === null || user === undefined)
		{
			result.send("User is null");
			return;
		}
		else if (user.token === "-1")
		{
			result.send("User not logged");
			return;
		}
		else
		{
			user.calendar[date] = schedule;		
		}	
	});
	result.send(200).end();
});

router.get('/alarm', function(request, result) {
    var token = request.query.token;
    if (token === '-1') {
        result.status(401).end();
        return;
    }
    var users = db.get("users");
    users.findOne({token : token}, function(err, user) {
        if (err) {}
        else {
            if (user === null || user === undefined) {
                result.status(401).end();
                return;
            }
            var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
            var currentDate;
            for (var key in user.calendar) {
                currentDate = new Date(parseInt(key.split("-")[2]), parseInt(key.split("-")[1]) - 1, parseInt(key.split("-")[0]));
                if (currentDate.toDateString() === today.toDateString()) {
                    currentDate  = currentDate.getDate() + "-" + (currentDate.getMonth() + 1) + "-" + currentDate.getFullYear();
                    for (var i = 0; i < 48; i++) {
                        if (JSON.stringify(user.calendar[currentDate][i]) !== "{}" && 
                            i >= new Date().getHours() * 2 + (new Date().getMinutes() >= 30 ? 1 : 0)) {
                            if (user.calendar[currentDate][i].alarm.checked === true) {
                                if (user.calendar[currentDate][i].alarm.delay + new Date().getMinutes() + new Date().getHours() * 60 >= i * 30 &&
                                    new Date().getMinutes() + new Date().getHours() * 60 <= i * 30 && user.calendar[currentDate][i].title !== undefined
                                    && user.calendar[currentDate][i].alarmRang === undefined) { 
                                    user.calendar[currentDate][i].alarmRang = true;
                                    users.update({token : token}, {$set : {calendar : user.calendar}});
                                    result.send({
                                        taskType : user.calendar[currentDate][i].taskType,
                                        note : user.calendar[currentDate][i].note,
                                        title : user.calendar[currentDate][i].title
                                    });
                                    return;
                                }
                            }
                        }
                    }
                }
            }
            result.send({});
        }
    });
});
router.get('/reminder', function(request, result) {
    var token = request.query.token;
    if (token === '-1') {
        result.status(401).end();
        return;
    }
    var users = db.get("users");
    users.findOne({token : token}, function(err, user) {
        if (err) {}
        else {
            if (user === null || user === undefined) {
                result.status(401).end();
                return;
            }
            var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
            var currentDate;
            for (var key in user.calendar) {
                currentDate = new Date(parseInt(key.split("-")[2]), parseInt(key.split("-")[1]) - 1, parseInt(key.split("-")[0]));
                if (currentDate.toDateString() === today.toDateString()) {
                    currentDate  = currentDate.getDate() + "-" + (currentDate.getMonth() + 1) + "-" + currentDate.getFullYear();
                    for (var i = 0; i < 48; i++) {
                        if (JSON.stringify(user.calendar[currentDate][i]) !== "{}" && 
                            i >= new Date().getHours() * 2 + (new Date().getMinutes() >= 30 ? 1 : 0)) {
                            if (user.calendar[currentDate][i].reminder.checked === true) {
                                if (user.calendar[currentDate][i].reminder.delay + new Date().getMinutes() + new Date().getHours() * 60 >= i * 30 &&
                                    new Date().getMinutes() + new Date().getHours() * 60 <= i * 30 && user.calendar[currentDate][i].title !== undefined
                                    && user.calendar[currentDate][i].reminderRang === undefined) { 
                                    user.calendar[currentDate][i].reminderRang = true;
                                    users.update({token : token}, {$set : {calendar : user.calendar}});
                                    result.send({
                                        taskType : user.calendar[currentDate][i].taskType,
                                        note : user.calendar[currentDate][i].note,
                                        title : user.calendar[currentDate][i].title
                                    });
                                    return;
                                }
                            }
                        }
                    }
                }
            }
            result.send({});
        }
    });
});

router.post('/dotask', function(request, result) {
    var token = request.body.token;
    if (token === '-1') {
        result.status(401).end();
        return;
    }
    var date = request.body.date;
    var start = request.body.start;
    var stop = request.body.stop;
    var users = db.get("users");
    users.findOne({token : token}, function(err, user) {
        if (err) {}
        else {
            if (user === null || user === undefined) {
                result.status(401).end();
                return;
            }
            var auxCalendar = user.calendar;
            start = Math.floor(start / 30);
            stop = Math.floor(stop / 30);
            for (var i = start; i < stop; i++) {
                auxCalendar[date][i].done = true;
            }
            users.update({token : token}, {$set : {calendar : auxCalendar}});
            result.status(200).end();
        }
    });
});

router.post("/canceltask", function(request, result) {
    var token = request.body.token;
    if (token === '-1') {
        result.status(401).end();
        return;
    }
    var date = request.body.date;
    var start = request.body.start;
    start = parseInt(start);
    var stop = request.body.stop;
    stop = parseInt(stop);
    var users = db.get("users");
    users.findOne({token : token}, function(err, user) {
        if (err) {}
        else {
            if (user === null || user === undefined) {
                result.status(401).end();
                return;
            }
            var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
            for (var key in user.calendar) {
                var currentDate = new Date(parseInt(key.split("-")[2]), parseInt(key.split("-")[1]) - 1, parseInt(key.split("-")[0]));
                if (currentDate.toDateString() === date) {
                    for (var i = start; i < stop; i++) {
                        user.calendar[key][i] = {};
                    }
                    break;
                }
            }
            users.update({token : token}, {$set : {calendar : user.calendar}});
        }
    });
});

router.post('/newtask', function(request, result) {
    var token = request.body.token;
    if (token === '-1') {
        result.status(401).end();
        return;
    }
    var date = request.body.date;
    var start = request.body.start;
    start = parseInt(start);
    var stop = request.body.stop;
    stop = parseInt(stop);
    var alarmChecked = request.body.alarmChecked;
    try {
        alarmChecked = JSON.parse(alarmChecked);
    } catch(e) {}
    var delayAlarm = request.body.alarmDelay;
    delayAlarm = parseInt(delayAlarm);
    var reminderChecked = request.body.reminderChecked;
    try {
        reminderChecked = JSON.parse(reminderChecked);
    } catch(e) {}
    var delayReminder = request.body.reminderDelay;
    delayReminder = parseInt(delayReminder);
    var taskType = request.body.taskType;
    var note = request.body.note;
    var title = request.body.title;
    var users = db.get('users');
    users.findOne({token : token}, function(err, user) {
        if (err) {}
        else {
            if (user === null || user === undefined) {
                result.status(401).end();
                return;
            }
            var auxCalendar = user.calendar;
            var objectToPush = {
                taskType : taskType,
                note : note,
                done : false,
                alarm : {
                    checked : alarmChecked,
                    delay : delayAlarm
                },
                reminder : {
                    checked : reminderChecked,
                    delay : delayReminder
                },
                isOccupied : true
            };
            var i;
            if (auxCalendar[date] === undefined) {
                auxCalendar[date] = [];
                for (i = 0; i < 48; i++) {
                    auxCalendar[date][i] = {
                        isOccupied : false
                    };
                }
            }
            var firstObjectToPush = {
                taskType : objectToPush.taskType,
                note : objectToPush.note,
                done : false,
                alarm : objectToPush.alarm,
                reminder : objectToPush.reminder,
                title : title
            };
            auxCalendar[date][start] = firstObjectToPush;
            //firstObjectToPush.title = null;
            for (i = start + 1; i < stop; i++) {
                auxCalendar[date][i] = objectToPush;
            }
            users.update({token : token}, {$set : {calendar : auxCalendar}});
            result.status(200).end();
        }
    });
});

module.exports = router;