var express = require('express');
var router = express.Router();
var monk = require('monk');
var mongoUri = 'localhost:27017/eeStecDB';
var db = monk(mongoUri);

router.get('/', function(request, result) {
  	var users = db.get('users');
  	users.find({}, function(err, allUsers) {
  		if (err) {}
  		else {
  			if (allUsers === null || allUsers.length === 0) {
  				result.send(404).end();
  				return;
  			}
  			var taskTypes = ["Petrecere", "Intalnire", "Curs", "Sport", "Altele"];
  			var scores = [5, 5, 10, 10, 2];
  			var usersScore = [];
  			for (var i = 0; i < allUsers.length; i++) {
  				usersScore[i] = {
  					score : 0,
  					username : allUsers[i].username,
  					firstName : allUsers[i].firstName,
  					lastName : allUsers[i].lastName
  				};
  				var previousWeekDate = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() - 7);
  				for (var key in allUsers[i].calendar) {
  					var currentDate = new Date(parseInt(key.split("-")[2]), parseInt(key.split("-")[1] - 1), parseInt(key.split("-")[0]));
  					if (currentDate > previousWeekDate) {
  						for (var k = 0; k < 48; k++) {
  							if (allUsers[i].calendar[key][k].title !== undefined) {
  								if (allUsers[i].calendar[key][k].done === true) {
  									for (var l = 0; l < taskTypes.length; l++) {
  										if (allUsers[i].calendar[key][k].taskType === taskTypes[l]) {
  											usersScore[i].score += scores[l];
  										}
  									}
  								}
  								k++;
  							}
  						}
  					}
  				}
  			}
  			usersScore.sort(function(a, b) {
  				return a.score < b.score;
  			});
  			result.send(usersScore);
  		}
  	});
});

module.exports = router;
