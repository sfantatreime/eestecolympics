var express = require('express');
var router = express.Router();
var monk = require('monk');
var mongoUri = 'localhost:27017/eeStecDB';
var db = monk(mongoUri);


function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex ;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

/* GET home page. */
router.get('/', function(request, result) {
	var users = db.get('users');
	var token = request.query.token;
	var preference = request.query.preference;
	var cap = request.query.cap;
	var ret = [];
	if (token === null || token === undefined)
	{
		result.send("Token is not defined");
		return ;
	}

	if (preference === null || preference === undefined)
	{
		result.send("Preference is not defined");
		return ;
	}

	console.log("Preference is " + preference);

	var prefDB = db.get(preference);
	if (prefDB === null || prefDB === undefined)
	{
		result.send("Preference not available");
		return ;
	}

	users.findOne ({token : token}, function(err, user)
	{
		if (err)
		{
			result.send(err);
			return ;
		}
		else if (user === null || user === undefined)
		{
			result.send("User is not defined");
			return ;
		}

		var ok = false;
		for (var z in user.prefs)
		{
			if (z === preference)
			{
				ok = true;
				break;
			}
		}
		console.log("Ok is " + ok);
		if (ok)
		{
			var prefs = user.prefs[preference];
			var tagsAp = [];
		
			console.log("prefs is " + prefs);

			var found = false;
			var keys = [];
			for (var i in prefs)
			{
				console.log("i is " + i);
				for (var tag in prefs[i].tags)
				{
					console.log("tag is " + tag);
					for (var k in keys)
					{
						if (k === prefs[i].tags[tag])
						{
							found = true;
						}
					}
					if (found === true)
					{
						tagsAp[prefs[i].tags[tag]]++;
					}
					else
					{
						tagsAp[prefs[i].tags[tag]] = 1;
						keys.push(prefs[i].tags[tag]);
					}
					found = false;
				}
			}
			var maxKey;
			var max = -1;
			for (var v in tagsAp)
			{
				if (tagsAp[v] > max)
				{
					maxKey = v;
					max = tagsAp[v];
				}
			}

			console.log("MaxKey is " + maxKey);

			prefDB.find({}, function(err, rec)
			{
				var recommended = [];
				var found = false;
				for (var r in rec)
				{
					for (var visited in prefs)
					{
						if (r.name === prefs[visited].name)
						{
							found = true;
						}
					}
					if (found === false)
					{
						for (var p in rec[r].tags)
						{
							if (rec[r].tags[p] === maxKey)
							{
								recommended.push(rec[r]);
								break;
							}
						}
					}
					found = false;
				}
				//recommended = shuffle(recommended);
				
				for (var i = 0; i < cap; i++)
				{
					ret.push(recommended[i]);
				}
				result.send(ret);
				return;
			});
		}
		else
		{
			prefDB.find({}, function(err, rec)
			{
				var recommended = rec;
				recommended = shuffle(recommended);
				for (var i = 0; i < cap; i++)
				{
					ret.push(recommended[i]);
				}
				result.send(ret);
				return ;

			});	
			return ;
		}
	});
});

router.post('/', function(request, result)
{
	var users = db.get('users');
	var token = request.query.token;
	var preference = request.query.preference;
	var obj = request.query.obj;
	if (token === null || token === undefined)
	{
		result.send("Token is not defined");
		return ;
	}

	if (preference === null || preference === undefined)
	{
		result.send("Preference is not defined");
		return ;
	}

	if (obj === null || obj === undefined)
	{
		result.send("Object to add is not defined");
		return ;
	}

	users.findOne({token : token},  function(err, user)
	{
		var prefs = [];
		var ok = false;
		for (var z in user.prefs)
		{
			if (z === preference)
			{
				ok = true;
				break;
			}
		}
		if (ok === false)
		{
			prefs = user.prefs;
			prefs[preference].push(obj);
			users.update({token : token}, {$set : {prefs : prefs}}, 
				function(err, number)
				{});

			return ;
		}
		else
		{
			prefs = user.prefs[preference];
			for (var k in prefs)
			{
				if (prefs[k].name === obj.name)
				{
					return ;
				}
			}
			prefs.push(obj);
			user.prefs[preference] = prefs;
			users.update({token : token}, {$set : {prefs : user.prefs}}, 
				function(err, number)
				{});
			return ;
		}
	});

});

router.get("/unseen", function(request, result) {
	var users = db.get('users');
	var token = request.query.token;
	var preference = request.query.preference;
	if (token === null || token === undefined)
	{
		result.send("Token is not defined");
		return ;
	}

	if (preference === null || preference === undefined)
	{
		result.send("Preference is not defined");
		return ;
	}

	var prefDB = db.get(preference);
	if (prefDB === null || prefDB === undefined)
	{
		result.send("Preference not available");
		return ;
	}

	users.findOne ({token : token}, function(err, user)
	{
		if (err)
		{
			result.send(err);
			return ;
		}
		else if (user === null || user === undefined)
		{
			result.send("User is not defined");
			return ;
		}

		var ok = false;
		for (var z in user.prefs)
		{
			if (z === preference)
			{
				ok = true;
				break;
			}
		}
		if (ok)
		{
			var found = false;
			var shown = [];
			var prefs = user.prefs[preference];
			prefDB.find({}, function(err, all)
				{
					var ret = [];
				
					for (var m in prefs)
					{
						if (ret.length >= 25)
							break;
						for (var n in all)
						{
							if (all[n].name === prefs[m].name)
							{
								found = true; 
							}
							if (!found)
							{
								ret.push(all[n]);
							}
							found = false;
						}
					}
					result.send(ret);
					return ;
				});
		}
		else
		{
			prefDB.find({}, function(err,all)
			{
				for (var q = 0; q < 25; q++)
				{
					ret.push(all[q]);
				}
				ret = shuffle(ret);
				result.send(ret);
				return ;
			});
		}
	});

});

<<<<<<< HEAD
module.exports = router;
=======
module.exports = router;

>>>>>>> a0f413724bbd5c51490299c7a55a99156b21dd47
