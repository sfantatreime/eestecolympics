var express = require('express');
var router = express.Router();
var monk = require('monk');
var mongoUri = 'localhost:27017/eeStecDB';
var db = monk(mongoUri);

/* GET home page. */
router.get('/', function(request, result) {
  	var token = request.query.token;
  	var users = db.get('users');
  	var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());

  	if (token === undefined || token === null)
  	{
  		result.send("Token is undefined");
  	}
  	var totalTasks = 0;
  	var solvedTasks = 0;
  	users.findOne({token : token}, function(err, user)
  	{
  		if (user === null || user === undefined)
  		{
  			result.send("User is not defined");
  			return ;
  		}	

  		for (var key in user.calendar)
  		{
  			var date = new Date(parseInt(key.split("-")[2]), parseInt(key.split("-")[1]) - 1, parseInt(key.split("-")[0]));
  			var timeDiff = today.getTime() - date.getTime();
  			var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
  			console.log("Difference in days is " + diffDays);
  			if (diffDays >= 0 && diffDays <= 7)
  			{
  				for (var v in  user.calendar[key])
  				{
  					if (JSON.stringify(user.calendar[key][v]) !== "{}" && user.calendar[key][v].title !== "")
  					{
  						if (user.calendar[key][v].done === true)
  						{
  							solvedTasks++;
  						}
  						totalTasks++;
  					} 
  				}
  			}
  		}

  		if (totalTasks === 0)
  		{
  			result.send({procent : totalTasks});
  			return;
  		}
  		else
  		{
  			result.send({procent : solvedTasks/totalTasks});
  			return ;
  		}
  	});
 });
module.exports = router;