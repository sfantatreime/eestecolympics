var express = require('express');
var router = express.Router();
var crypto = require("crypto");
var monk = require('monk');
var mongoUri = 'localhost:27017/eeStecDB';
var db = monk(mongoUri);

/* GET home page. */
router.post('/', function(request, result) {
  	var firstName = request.body.firstName;
  	var lastName = request.body.lastName;
  	var username = request.body.username;
  	var password = request.body.password;
  	var email = request.body.email;
  	var users = db.get('users');
  	users.findOne({username : username}, function(err, user) {
  		if (err) {}
  		if (user !== null && user !== undefined) {
  			result.send("Username-ul exista deja!");
  			return;
  		}
  		else {
  			crypto.pbkdf2(password, 'ala-bala-portocala', 10000, 20, function(err, derivedKey) {
                if (err) {}
                else {
                    users.insert({
                    	username : username,
                    	password : derivedKey,
                    	firstName : firstName,
                    	lastName : lastName,
                    	email : email,
                    	calendar : {},
                    	mood : "",
                    	prefs : {},
                    	token : "-1"
                    });
                    result.send(200).end();
                }
            });
  		}
  	});
});

module.exports = router;
