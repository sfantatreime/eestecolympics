'use strict';

/**
 * @ngdoc directive
 * @name eeStecOlympicsFrontendApp.directive:navbar
 * @description
 * # navbar
 */
angular.module('eeStecOlympicsFrontendApp')
  .directive('navbar',
    ['$state',
    '$interval',
    'AuthService',
    'CalendarService',

    function ($state, $interval, AuthService, CalendarService) {
      return {
        templateUrl: 'views/navbar.html',
        restrict: 'E',
        link: function postLink(scope, element, attrs) {
          scope.image = null;
          scope.message = '';

          var clearCredentials = function() {
            scope.credentials.username = '';
            scope.credentials.password = '';
          };

          var update = function() {
            alert('bravo ma')
          };

          scope.user = null;

          AuthService.getUserDetails()
          .then(function(data) {
            scope.user = data;
            console.log(scope.user);
          }, function(data) {
            scope.user = null;
          });

          var getMood = function() {
            AuthService.getMood()
            .then(function(data) {
              scope.image = getImage(data.procent);
            });
          }

          getMood();

          $interval(function() {
            CalendarService.getAlarm()
            .then(function(data) {
              if (data.hasOwnProperty(title) === true) {
                //t0d0 alarm
                scope.message = 'Alarma! Un eveniment incepe in curand!';
                alert($scope.message);
                $timeout(function() {
                  scope.message = '';
                }, 1000 * 60);
              }
            });

            CalendarService.getReminder()
            .then(function(data) {
              if (data.hasOwnProperty(title) === true) {
                //t0d0 reminder
                scope.message = 'Memento! Urmeaza un eveniment!';
                $timeout(function() {
                  scope.message = '';
                }, 1000 * 30);
              }
            });
          }, 1000 * 60);

          scope.credentials = {
            username: '',
            password: ''
          };

          scope.login = function() {
            AuthService.login(scope.credentials)
            .then(function(data) {
              clearCredentials();
              // update();
              $state.go('home.calendar');
            }, function() {
              alert('bad username or password');
            });
          };
  
          scope.logout = function() {
            AuthService.logout()
            .then(function() {
              scope.user = null;
              scope.image = '';
              $state.go('startingPage');
            }, function() {
              alert('Eroare la log out');
            });
          };

          scope.$on('gotUserDetails', function(event, data) {
            console.log(data);
            scope.user = data;
          });

          scope.$on('getImage', function(event) {
            event.preventDefault();
            event.stopPropagation();

            scope.image = getMood(); 
          });   
        }
      };
    }
]);

function getImage(number) {
  console.log(number)
  for (var i = 0; i < 6; ++i) {
    if ((number * 100 >= i * (100 / 6)) && (number * 100 <= (i + 1) * (100 / 6))) {
      var count = 7 - (i + 1);
      console.log(count)
      return 'smiley' + count + '.png';
    }
  }
};