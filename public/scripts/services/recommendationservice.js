'use strict';

/**
 * @ngdoc service
 * @name eeStecOlympicsFrontendApp.RecommendationService
 * @description
 * # RecommendationService
 * Factory in the eeStecOlympicsFrontendApp.
 */
angular.module('eeStecOlympicsFrontendApp')
  .factory('RecommendationService',
    ['$q',
    '$http',
    '$cookieStore',
    'SERVER',

    function ($q, $http, $cookieStore, SERVER) {
    // Service logic
    // ...

    // Public API here
    return {
      getRecommendations: function (preference, c) {
        var cap = c || 10;
        var deferred = $q.defer();
        var token = $cookieStore.get('token');

        $http.get(SERVER.URL + SERVER.PREFERENCES, {
          params: {
            token: token,
            preference: preference,
            cap: cap
          }
        }).success(function(data) {
          deferred.resolve(data);
        }).error(function(data) {
          deferred.reject(data);
        });
        return deferred.promise;
      },
      sendRecommendation: function(recommendation, preference) {
        var deferred = $q.defer();
        var token = $cookieStore.get('token');

        $http.post(SERVER.URL + SERVER.PREFERENCES, {
          token: token,
          preference: preference,
          obj: recommendation
        }).success(function(data) {
          deferred.resolve(data);
        }).error(function(data) {
          deferred.reject(data);
        });
        return deferred.promise;
      }
    };
  }
]);
