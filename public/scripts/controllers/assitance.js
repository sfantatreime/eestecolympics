'use strict';

/**
 * @ngdoc function
 * @name eeStecOlympicsFrontendApp.controller:AssitanceCtrl
 * @description
 * # AssitanceCtrl
 * Controller of the eeStecOlympicsFrontendApp
 */
angular.module('eeStecOlympicsFrontendApp')
  .controller('AssitanceCtrl',
  	['$scope',
  	'AssistanceService',

  	function ($scope, AssistanceService) {
    	$scope.list = [];
    	$scope.text = '';

    	$scope.ask = function() {
    		console.log('pikachu')
    		AssistanceService.askQuestion($scope.text)
    		.then(function(answer) {
    			$scope.list.push({
    				question: $scope.text,
    				answer: answer
    			});
    			$scope.text = '';
    		})
    	};
  	}
]);
