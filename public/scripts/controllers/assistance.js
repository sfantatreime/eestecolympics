'use strict';

/**
 * @ngdoc function
 * @name eeStecOlympicsFrontendApp.controller:AssistanceCtrl
 * @description
 * # AssistanceCtrl
 * Controller of the eeStecOlympicsFrontendApp
 */
angular.module('eeStecOlympicsFrontendApp')
  .controller('AssistanceCtrl',
  	['$scope',
  	'AssistanceService',

  	function ($scope, AssistanceService) {
    	$scope.list = [];
    	$scope.text = '';

    	$scope.ask = function() {
    		console.log('pikachu')
    		AssistanceService.askQuestion($scope.text)
    		.then(function(answer) {
    			$scope.list.unshift({
    				question: $scope.text,
    				answer: answer
    			});
    			$scope.text = '';
    		})
    	};
  	}
]);