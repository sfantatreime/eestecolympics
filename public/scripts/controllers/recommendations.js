'use strict';

/**
 * @ngdoc function
 * @name eeStecOlympicsFrontendApp.controller:RecommendationsCtrl
 * @description
 * # RecommendationsCtrl
 * Controller of the eeStecOlympicsFrontendApp
 */
angular.module('eeStecOlympicsFrontendApp')
  .controller('RecommendationsCtrl',
  	['$scope',
  	'RecommendationService',

  	function ($scope, RecommendationService) {
    	$scope.options = ['books', 'movies'];
    	var random = Math.floor($scope.options.length * Math.random());
    	$scope.preference = $scope.options[random];

    	RecommendationService.getRecommendations($scope.preference)
    	.then(function(data) {
    		$scope.list = data;
    	});

      $scope.addToFav = function(element) {
        RecommendationService.sendRecommendation(element, $scope.preference)
        .then(function(data) {
          
        });
      };
  	}
]);
