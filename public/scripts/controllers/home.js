'use strict';

/**
 * @ngdoc function
 * @name eeStecOlympicsFrontendApp.controller:HomeCtrl
 * @description
 * # HomeCtrl
 * Controller of the eeStecOlympicsFrontendApp
 */
angular.module('eeStecOlympicsFrontendApp')
  .controller('HomeCtrl',
  	['$scope',
    '$state',  	
    'AuthService',

  	function ($scope, $state, AuthService) {
  		$scope.user = null;

   		AuthService.getUserDetails()
   		.then(function(data) {
   			$scope.$emit('gotUserDetails', data);
   			$scope.user = data;	
   		}, function(error) {
        $state.go('startingPage');
      });
  	}
]);
