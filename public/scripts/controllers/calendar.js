'use strict';

/**
 * @ngdoc function
 * @name eeStecOlympicsFrontendApp.controller:CalendarCtrl
 * @description
 * # CalendarCtrl
 * Controller of the eeStecOlympicsFrontendApp
 */
angular.module('eeStecOlympicsFrontendApp')
  .controller('CalendarCtrl',
  	['$scope',
  	'$timeout',
    'BookingService',
  	'CalendarService',
    'DateService',

  	function ($scope, $timeout, BookingService, CalendarService, DateService) {
    	var date = new Date();
      var errorTimeout = null;

      $scope.errorMessage = '';
  		$scope.schedule = [];
      $scope.slots = [];
    	$scope.booking = null;
      $scope.alarmChecked = false;
      $scope.reminderChecked = false;
      $scope.bookingList = BookingService.getBookingList(); 
      $scope.taskList = $scope.bookingList[0];
      $scope.note = '';
      $scope.title = '';
      $scope.reminderDelay = null;
      $scope.alarmDelay = null;
      
      $scope.delayList = [{
        text: '0:15',
        value: 15
      }, {
        text: '0:30',
        value: 30
      }, {
        text: '0:45',
        value: 45
      }, {
        text: '1:00',
        value: 60
      }];


    	var getSlots = function() {
        var parsedDate = DateService.parseDate(date);
	    	CalendarService.getSlots(parsedDate)
	    	.then(function(data) {
	    		$scope.schedule = data;
          $scope.slots = parseSlots(data);
          $scope.$emit('getImage');
	    	});
    	};

    	$scope.saveBooking = function() {
    		var booking = {
          date: DateService.parseDate(date),
          start: $scope.booking.from,
          stop: $scope.booking.until,
          alarmChecked: $scope.alarmChecked,
          alarmDelay: $scope.alarmDelay === null ? 0 : $scope.alarmDelay.value,
          reminderChecked: $scope.reminderChecked,
          reminderDelay: $scope.reminderDelay === null ? 0 : $scope.reminderDelay.value,
          taskType: $scope.taskType,
          note: $scope.note,
          title: $scope.title
        };

        $scope.booking = null;

        BookingService.sendBooking(booking)
        .then(function() {
          $timeout(function() {
            getSlots();
          }, 1000);
        });
    	};

    	$scope.cancelBooking = function() {
				getSlots();
        $scope.booking = null;
        $scope.errorMessage = '';    		
    	};

      $scope.dismiss = function() {
        $scope.booking = null
      };

      $scope.cancel = function(task) {
        BookingService.cancelBooking(task)
        .then(function(data) {
          $scope.booking = null;
          getSlots();
        });
      };

      $scope.doneBooking = function(booking) {
        BookingService.doneBooking(booking)
        .then(function(data) {
          $scope.null;
          getSlots();
        })
      }

    	$scope.$on('bookingSelected', function(event, booking) {
    		event.preventDefault();
        event.stopPropagation();
    		console.log(booking)
    		$scope.booking = booking;
        $scope.booking.old = false;
    	});

    	$scope.$on('bookingDeselected', function(event) {
    		event.preventDefault();
        event.stopPropagation();
    		$scope.booking = null;
    	});

    	$scope.$on('resolveBookingError', function(event) {
    		event.preventDefault();
        event.stopPropagation();
    		$scope.errorMessage = 'Trebuie sa finalizezi un task inainte de a inregistra altul!';
    		errorTimeout = $timeout(function() {
    			$scope.errorMessage = '';
    		}, 4000);
    	});

      $scope.$on('newDate', function(event, newDate) {
        event.preventDefault();
        event.stopPropagation();
        date = newDate;
        getSlots();
      });

      $scope.$on('errorOnSlotSelect', function(event) {
        event.preventDefault();
        event.stopPropagation();
        $scope.errorMessage = 'Nu poti crea un task care se intersecteaza cu un altul!';
        errorTimeout = $timeout(function() {
          $scope.errorMessage = '';
        }, 4000);
        getSlots();
      });

      $scope.$on('bookingClicked', function(event, index) {
        event.preventDefault();
        event.stopPropagation();

        $scope.booking = getBooking($scope.schedule, index, DateService.parseDate(date));
        console.log('pulaaaaaaaa');
      });
  	}
]);

function parseSlots(schedule) {
  var slots = [];
  console.log('schedule:')
  console.log(schedule)
  for (var i = 0; i < schedule.length; ++i) {
    slots.push(false);
  }
  return slots;
};

function getBooking(slots, index, date) {
  for (var top = index; slots[top].title === undefined; --top){}
  for (var bottom = index; bottom < 48 && slots[bottom].isOccupied && slots[bottom].title === undefined; ++bottom){}
  return {
    title: slots[top].title,
    note: slots[top].note,
    from: top,
    until: bottom,
    date: date,
    old: true
  };
};