'use strict';

/**
 * @ngdoc overview
 * @name eeStecOlympicsFrontendApp
 * @description
 * # eeStecOlympicsFrontendApp
 *
 * Main module of the application.
 */
angular
  .module('eeStecOlympicsFrontendApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.router',
    'ui.bootstrap'
  ])
  .config(['$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {
      $urlRouterProvider.otherwise('startingPage');
      $urlRouterProvider.when('', 'startingPage');

      $stateProvider
        .state('navbar', {
          url: '',
          template: '<navbar></navbar><ui-view/>'
        })
        .state('startingPage', {
          url: '/startingpage',
          templateUrl: 'views/startingpage.html',
          controller: 'StartingpageCtrl',
          parent: 'navbar'
        })
        .state('home', {
          url: '/home',
          templateUrl: 'views/home.html',
          controller: 'HomeCtrl',
          parent: 'navbar'
        })
        .state('home.calendar', {
          url: '/calendar',
          templateUrl: 'views/calendar.html',
          controller: 'CalendarCtrl'
        })
        .state('home.assistance', {
          url: '/assistance',
          templateUrl: 'views/assistance.html',
          controller: 'AssistanceCtrl'
        })
        .state('home.recommendations', {
          url: '/recommendations',
          templateUrl: 'views/recommendations.html',
          controller: 'RecommendationsCtrl'
        })
        .state('home.bored', {
          url: '/bored',
          templateUrl: 'views/bored.html',
          controller: ''
        })
        .state('home.rankings', {
          url: '/rankings',
          templateUrl: 'views/rankings.html',
          controller: 'RankingsCtrl'
        })
        .state('home.help', {
          url: '/help',
          templateUrl: 'views/help.html',
          controller: ''
        })
        .state('home.alarm', {
          url: '/alarm',
          templateUrl: 'views/alarm.html',
          controller: 'AlarmCtrl'
        })
        .state('signup', {
          url: '/signup',
          templateUrl: 'views/signup.html',
          controller: 'SignupCtrl'
        })
    }
]);
