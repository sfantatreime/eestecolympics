'use strict';

/**
 * @ngdoc filter
 * @name eeStecOlympicsFrontendApp.filter:toHour
 * @function
 * @description
 * # toHour
 * Filter in the eeStecOlympicsFrontendApp.
 */
angular.module('eeStecOlympicsFrontendApp')
  .filter('toHour', function () {
    return function (input) {
    	var hour = Math.floor(input / 2);
    	var prefix = Math.floor(hour / 10) == 0 ? '0' : '';
      return '' + prefix + hour + ':' + (input % 2) * 3 + '0';
    };
  });
