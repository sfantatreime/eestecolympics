var express = require('express');
var http = require('http');
var path = require('path');
//var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var monk = require('monk');
var mongoUri = 'localhost:27017/eeStecDB';
var db = monk(mongoUri);
var cors = require('cors');

var routes = require('./routes/index');
var calendar = require('./routes/calendar');
var login = require("./routes/login");
var logout = require("./routes/logout");
var signup = require("./routes/signup");
var assistant = require("./routes/assistant");
var userinfo = require('./routes/userinfo');
var preferences = require("./routes/preferences");
var mood = require("./routes/mood");
var clasament = require("./routes/clasament");

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.set('port', 1337);

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(cors());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/calendar', calendar);
app.use('/login', login);
app.use('/logout', logout);
app.use('/signup', signup);
app.use('/assistant', assistant);
app.use('/userinfo', userinfo);
app.use('/preferences', preferences);
app.use('/mood', mood);
app.use('/clasament', clasament);

var log4js = require('log4js'); // include log4js
log4js.configure({ // configure to use all types in different files.
    appenders: [
        {   
        	type: "file",
            filename: "./error.log", // specify the path where u want logs folder info.log
            category: 'error'
        }
    ]
});

var errorLogger = log4js.getLogger('error'); // initialize the error logger
/*(function() {
	req = require("request");
	//p2v6nydjvmd78q7pa7j9tbkd - movies review
	var pathForRequest = "http://api.usatoday.com/open/reviews/movies/movies?api_key=p2v6nydjvmd78q7pa7j9tbkd";
	req(pathForRequest, function(error, response, body) {
		var movies = db.get("movies");
		console.log(response.statusCode);
	    if ( !error && response.statusCode === 200 ) {
	    	var moviesAPI = JSON.parse(body);
	    	console.log(moviesAPI);
	    }
	});
})();*/



//fara liniile astea 3, nimic nu s-ar intampla...
//porneste serverul
http.createServer(app).listen(app.get('port'), function(){
    console.log('Server listening on port ' + app.get('port'));
});

module.exports = app;
